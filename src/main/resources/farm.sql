/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50703
Source Host           : localhost:3306
Source Database       : farm

Target Server Type    : MYSQL
Target Server Version : 50703
File Encoding         : 65001

Date: 2020-07-08 22:48:54
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `jsh_account`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_account`;
CREATE TABLE `jsh_account` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `SerialNo` varchar(50) DEFAULT NULL COMMENT '编号',
  `InitialAmount` decimal(24,6) DEFAULT NULL COMMENT '期初金额',
  `CurrentAmount` decimal(24,6) DEFAULT NULL COMMENT '当前余额',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `IsDefault` bit(1) DEFAULT NULL COMMENT '是否默认',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='账户信息';

-- ----------------------------
-- Records of jsh_account
-- ----------------------------
INSERT INTO `jsh_account` VALUES ('4', '南通建行', '652346523465234623', '1200.000000', '215.000000', '建行账户', '\0', null, '1');
INSERT INTO `jsh_account` VALUES ('9', '流动总账', '65234624523452364', '2000.000000', '393.000000', '现在账户', '', null, '0');
INSERT INTO `jsh_account` VALUES ('10', '支付宝', '123456789@qq.com', '10000.000000', null, '', '\0', null, '0');
INSERT INTO `jsh_account` VALUES ('11', '微信', '13000000000', '10000.000000', null, '', '\0', null, '0');
INSERT INTO `jsh_account` VALUES ('12', '上海农行', '65324345234523211', '10000.000000', '0.000000', '', '\0', null, '0');
INSERT INTO `jsh_account` VALUES ('13', '账户1', 'abcd123', '0.000000', null, '', '', '1', '0');
INSERT INTO `jsh_account` VALUES ('14', '账户1', 'zhanghu1', '0.000000', null, '', '', '117', '0');
INSERT INTO `jsh_account` VALUES ('15', '账户2222', 'zh2222', '0.000000', null, '', '\0', '117', '0');
INSERT INTO `jsh_account` VALUES ('16', '账户1', '1231241244', '0.000000', null, '', '', '115', '0');
INSERT INTO `jsh_account` VALUES ('17', '账户1', 'zzz111', '0.000000', null, '', '', '63', '0');
INSERT INTO `jsh_account` VALUES ('18', '账户2', '1234131324', '0.000000', null, '', '\0', '63', '0');
INSERT INTO `jsh_account` VALUES ('19', '账户3', '003', '100000.000000', null, '', '\0', '63', '0');
INSERT INTO `jsh_account` VALUES ('20', '中国邮政', '62636300000', '190000.000000', null, '', '\0', null, '0');
INSERT INTO `jsh_account` VALUES ('21', '建设银行', '6232329389', '1000000.000000', null, '', '\0', null, '0');

-- ----------------------------
-- Table structure for `jsh_accounthead`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accounthead`;
CREATE TABLE `jsh_accounthead` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Type` varchar(50) DEFAULT NULL COMMENT '类型(支出/收入/收款/付款/转账)',
  `OrganId` bigint(20) DEFAULT NULL COMMENT '单位Id(收款/付款单位)',
  `HandsPersonId` bigint(20) DEFAULT NULL COMMENT '经手人Id',
  `ChangeAmount` decimal(24,6) DEFAULT NULL COMMENT '变动金额(优惠/收款/付款/实付)',
  `TotalPrice` decimal(24,6) DEFAULT NULL COMMENT '合计金额',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户(收款/付款)',
  `BillNo` varchar(50) DEFAULT NULL COMMENT '单据编号',
  `BillTime` datetime DEFAULT NULL COMMENT '单据日期',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`),
  KEY `FK9F4C0D8DB610FC06` (`OrganId`),
  KEY `FK9F4C0D8DAAE50527` (`AccountId`),
  KEY `FK9F4C0D8DC4170B37` (`HandsPersonId`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='财务主表';

-- ----------------------------
-- Records of jsh_accounthead
-- ----------------------------
INSERT INTO `jsh_accounthead` VALUES ('57', '收预付款', '8', '3', null, '1000.000000', null, '2342134', '2017-06-27 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('61', '收预付款', '9', '3', null, '33.000000', null, 'SYF2017062901721', '2017-06-29 00:00:00', 'aaaaaa', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('67', '收预付款', '10', '4', null, '2100.000000', null, 'SYF2017070222414', '2017-07-02 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('70', '支出', '4', '3', '-60.000000', '-60.000000', '4', 'ZC20170703233735', '2017-07-03 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('74', '转账', null, '3', '-100.000000', '-100.000000', '4', 'ZZ2017070323489', '2017-07-03 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('77', '收入', '2', '3', '40.000000', '40.000000', '4', 'SR20170704222634', '2017-07-04 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('78', '收预付款', '9', '3', null, '200.000000', null, 'SYF201707050257', '2017-07-05 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('79', '收预付款', '9', '3', null, '100.000000', null, 'SYF20170705076', '2017-07-05 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('82', '收款', '2', '3', '0.000000', '2.600000', null, 'SK20171008191440', '2017-10-09 00:08:11', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('83', '付款', '1', '4', '0.000000', '-20.000000', null, 'FK20171008232825', '2017-10-08 00:00:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('84', '收入', '2', '4', '0.000000', '21.000000', '10', 'SR20171009000300', '2017-10-09 00:03:00', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('85', '收入', '2', '3', '22.000000', '22.000000', '11', 'SR20171009000637', '2017-10-09 00:06:37', '备注123 备注123 备注123', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('86', '转账', null, '4', '-22.000000', '-22.000000', '10', 'ZZ20171009000719', '2017-10-09 00:07:19', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('87', '付款', '4', '4', '10.000000', '-33.000000', null, 'FK20171009000747', '2017-10-09 00:07:47', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('88', '收款', '2', '4', '0.000000', '2.800000', null, 'SK20171024220754', '2017-10-24 22:07:54', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('89', '收款', '2', '4', '0.000000', '11.000000', null, 'SK20171030232535', '2017-10-30 23:25:35', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('90', '收款', '2', '4', '0.000000', '10.000000', null, 'SK20171119231440', '2017-11-19 23:14:40', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('91', '收入', '48', '9', '66.000000', '6.000000', '13', 'SR20190319221438', '2019-03-19 22:14:38', '', '1', '0');
INSERT INTO `jsh_accounthead` VALUES ('92', '支出', '50', '9', '-33.000000', '-33.000000', '13', 'ZC20190319221454', '2019-03-19 22:14:54', '', '1', '0');
INSERT INTO `jsh_accounthead` VALUES ('93', '收款', '48', '9', null, '44.000000', null, 'SK20190319221513', '2019-03-19 22:15:13', '', '1', '0');
INSERT INTO `jsh_accounthead` VALUES ('94', '付款', '50', '9', null, '-66.000000', null, 'FK20190319221525', '2019-03-19 22:15:25', '', '1', '0');
INSERT INTO `jsh_accounthead` VALUES ('95', '收预付款', '49', '9', null, '6.000000', null, 'SYF20190319221556', '2019-03-19 22:15:56', '', '1', '0');
INSERT INTO `jsh_accounthead` VALUES ('96', '收入', '5', '4', '22.000000', '22.000000', '12', 'SR20190321235925', '2019-03-21 23:59:25', '', null, '0');
INSERT INTO `jsh_accounthead` VALUES ('97', '收入', '58', '16', '10.000000', '10.000000', '17', 'SR20191228121609', '2019-12-28 12:16:09', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('98', '支出', '57', '16', '-20.000000', '-20.000000', '17', 'ZC20191228121854', '2019-12-28 12:18:54', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('99', '收款', '58', '16', null, '20.000000', null, 'SK20191228121908', '2019-12-28 12:19:08', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('100', '付款', '68', '16', null, '-20.000000', null, 'FK20191228121920', '2019-12-28 12:19:20', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('101', '转账', null, '16', '-20.000000', '-20.000000', '18', 'ZZ20191228121932', '2019-12-28 12:19:32', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('102', '收预付款', '60', '16', null, '1000.000000', null, 'SYF20191228121945', '2019-12-28 12:19:45', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('103', '收入', '59', '16', '1111.000000', '111.000000', '18', 'SR20200708101053', '2020-07-08 10:10:53', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('104', '支出', '68', '16', '-1.000000', '-1.000000', '17', 'ZC20200708101217', '2020-07-08 10:12:17', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('105', '收款', '76', '16', '111.000000', '111.000000', null, 'SK20200708101248', '2020-07-08 10:12:48', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('106', '付款', '77', '16', '1111.000000', '-111.000000', null, 'FK20200708101312', '2020-07-08 10:13:12', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('107', '转账', null, '16', '-11.000000', '-11.000000', '17', 'ZZ20200708101340', '2020-07-08 10:13:40', '', '63', '0');
INSERT INTO `jsh_accounthead` VALUES ('108', '收预付款', '60', '16', '11.000000', '11.000000', null, 'SYF20200708101407', '2020-07-08 10:14:07', '', '63', '0');

-- ----------------------------
-- Table structure for `jsh_accountitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_accountitem`;
CREATE TABLE `jsh_accountitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户Id',
  `InOutItemId` bigint(20) DEFAULT NULL COMMENT '收支项目Id',
  `EachAmount` decimal(24,6) DEFAULT NULL COMMENT '单项金额',
  `Remark` varchar(100) DEFAULT NULL COMMENT '单据备注',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`),
  KEY `FK9F4CBAC0AAE50527` (`AccountId`),
  KEY `FK9F4CBAC0C5FE6007` (`HeaderId`),
  KEY `FK9F4CBAC0D203EDC5` (`InOutItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8 COMMENT='财务子表';

-- ----------------------------
-- Records of jsh_accountitem
-- ----------------------------
INSERT INTO `jsh_accountitem` VALUES ('58', '57', '9', null, '1000.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('62', '61', '4', null, '33.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('68', '67', '4', null, '2100.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('71', '70', null, '11', '60.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('75', '74', '9', null, '100.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('78', '77', null, '14', '40.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('79', '78', '9', null, '200.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('80', '79', '9', null, '100.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('83', '82', '10', null, '2.600000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('84', '83', '10', null, '-20.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('85', '84', null, '13', '21.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('86', '85', null, '12', '22.000000', '44', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('87', '86', '11', null, '22.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('88', '87', '10', null, '-33.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('89', '88', '10', null, '2.800000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('90', '89', '11', null, '11.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('91', '90', '12', null, '10.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('92', '91', null, '16', '66.000000', '', '1', '0');
INSERT INTO `jsh_accountitem` VALUES ('93', '92', null, '17', '33.000000', '', '1', '0');
INSERT INTO `jsh_accountitem` VALUES ('94', '93', '13', null, '44.000000', '', '1', '0');
INSERT INTO `jsh_accountitem` VALUES ('95', '94', '13', null, '-66.000000', '', '1', '0');
INSERT INTO `jsh_accountitem` VALUES ('96', '95', '13', null, '6.000000', '', '1', '0');
INSERT INTO `jsh_accountitem` VALUES ('97', '96', null, '14', '22.000000', '', null, '0');
INSERT INTO `jsh_accountitem` VALUES ('98', '97', null, '22', '10.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('99', '98', null, '21', '20.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('100', '99', '17', null, '20.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('101', '100', '17', null, '-20.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('102', '101', '17', null, '20.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('103', '102', '17', null, '1000.000000', '', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('104', '105', '111', null, '111.000000', '111', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('105', '106', '1111', null, '-111.000000', '111', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('106', '107', '18', null, '11.000000', '11', '63', '0');
INSERT INTO `jsh_accountitem` VALUES ('107', '108', '17', null, '11.000000', '11', '63', '0');

-- ----------------------------
-- Table structure for `jsh_depot`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depot`;
CREATE TABLE `jsh_depot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) DEFAULT NULL COMMENT '仓库名称',
  `address` varchar(50) DEFAULT NULL COMMENT '仓库地址',
  `warehousing` decimal(24,6) DEFAULT NULL COMMENT '仓储费',
  `truckage` decimal(24,6) DEFAULT NULL COMMENT '搬运费',
  `type` int(10) DEFAULT NULL COMMENT '类型',
  `sort` varchar(10) DEFAULT NULL COMMENT '排序',
  `remark` varchar(100) DEFAULT NULL COMMENT '描述',
  `principal` bigint(20) DEFAULT NULL COMMENT '负责人',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `is_default` bit(1) DEFAULT NULL COMMENT '是否默认',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='仓库表';

-- ----------------------------
-- Records of jsh_depot
-- ----------------------------
INSERT INTO `jsh_depot` VALUES ('1', '叠石桥店', '地址222', '33.000000', '22.000000', '0', '2', '上海33', '95', null, '0', null);
INSERT INTO `jsh_depot` VALUES ('2', '公司总部', '地址12355', '44.000000', '22.220000', '0', '1', '总部', '64', null, '0', null);
INSERT INTO `jsh_depot` VALUES ('3', '金沙店', '地址666', '31.000000', '4.000000', '0', '3', '苏州', '64', null, '0', null);
INSERT INTO `jsh_depot` VALUES ('4', '1268200294', '', null, null, '1', '1', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('5', '1268787965', null, null, null, '1', '3', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('6', '1269520625', null, null, null, '1', '2', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('16', '仓库1', '黑龙江', '1.000000', '1.000000', '0', '1', '1', null, '63', '0', null);
INSERT INTO `jsh_depot` VALUES ('17', '仓库2', '黑龙江', '2.000000', '2.000000', '0', '2', '22222222222', null, '63', '0', null);
INSERT INTO `jsh_depot` VALUES ('18', '仓库3', '黑龙江', '3.000000', '3.000000', '0', '3', '333333333', null, '63', '0', null);
INSERT INTO `jsh_depot` VALUES ('19', '中兴仓库', '中兴大道109号', '15000.000000', '1000.000000', '0', '', '', null, '63', '0', null);
INSERT INTO `jsh_depot` VALUES ('20', '仓库4', '仓库4', '4.000000', '4.000000', '0', '4', '44', '133', '63', '0', null);
INSERT INTO `jsh_depot` VALUES ('21', '海伦', '钟表社南二路口', '1500.000000', '200.000000', '0', '', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('22', '海伦', '钟表社南二路口', '1500.000000', '200.000000', '0', '', '', null, null, '1', null);
INSERT INTO `jsh_depot` VALUES ('23', '海兴农场', '海星镇华兴街1009号', '10000.000000', '300.000000', '0', '', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('24', '海发农场', '哈法镇102号', '10000.000000', '100.000000', '0', '', '', null, null, '1', null);
INSERT INTO `jsh_depot` VALUES ('25', '仓库新', '哈尔滨', '23423.000000', '500.000000', '0', '', '', null, null, '0', null);
INSERT INTO `jsh_depot` VALUES ('26', '哈尔滨农场', '哈尔滨西', '100000.000000', '7000.000000', '0', '', '', null, null, '0', null);

-- ----------------------------
-- Table structure for `jsh_depothead`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depothead`;
CREATE TABLE `jsh_depothead` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Type` varchar(50) DEFAULT NULL COMMENT '类型(出库/入库)',
  `SubType` varchar(50) DEFAULT NULL COMMENT '出入库分类',
  `DefaultNumber` varchar(50) DEFAULT NULL COMMENT '初始票据号',
  `Number` varchar(50) DEFAULT NULL COMMENT '票据号',
  `OperPersonName` varchar(50) DEFAULT NULL COMMENT '操作员名字',
  `CreateTime` datetime DEFAULT NULL COMMENT '创建时间',
  `OperTime` datetime DEFAULT NULL COMMENT '出入库时间',
  `OrganId` bigint(20) DEFAULT NULL COMMENT '供应商Id',
  `HandsPersonId` bigint(20) DEFAULT NULL COMMENT '采购/领料-经手人Id',
  `AccountId` bigint(20) DEFAULT NULL COMMENT '账户Id',
  `ChangeAmount` decimal(24,6) DEFAULT NULL COMMENT '变动金额(收款/付款)',
  `TotalPrice` decimal(24,6) DEFAULT NULL COMMENT '合计金额',
  `PayType` varchar(50) DEFAULT NULL COMMENT '付款类型(现金、记账等)',
  `Remark` varchar(1000) DEFAULT NULL COMMENT '备注',
  `Salesman` varchar(50) DEFAULT NULL COMMENT '业务员（可以多个）',
  `AccountIdList` varchar(50) DEFAULT NULL COMMENT '多账户ID列表',
  `AccountMoneyList` varchar(200) DEFAULT '' COMMENT '多账户金额列表',
  `Discount` decimal(24,6) DEFAULT NULL COMMENT '优惠率',
  `DiscountMoney` decimal(24,6) DEFAULT NULL COMMENT '优惠金额',
  `DiscountLastMoney` decimal(24,6) DEFAULT NULL COMMENT '优惠后金额',
  `OtherMoney` decimal(24,6) DEFAULT NULL COMMENT '销售或采购费用合计',
  `OtherMoneyList` varchar(200) DEFAULT NULL COMMENT '销售或采购费用涉及项目Id数组（包括快递、招待等）',
  `OtherMoneyItem` varchar(200) DEFAULT NULL COMMENT '销售或采购费用涉及项目（包括快递、招待等）',
  `AccountDay` int(10) DEFAULT NULL COMMENT '结算天数',
  `Status` varchar(1) DEFAULT '0' COMMENT '状态，0未审核、1已审核、2已转采购|销售',
  `LinkNumber` varchar(50) DEFAULT NULL COMMENT '关联订单号',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`),
  KEY `FK2A80F214C4170B37` (`HandsPersonId`),
  KEY `FK2A80F214B610FC06` (`OrganId`),
  KEY `FK2A80F214AAE50527` (`AccountId`)
) ENGINE=InnoDB AUTO_INCREMENT=282 DEFAULT CHARSET=utf8 COMMENT='单据主表';

-- ----------------------------
-- Records of jsh_depothead
-- ----------------------------
INSERT INTO `jsh_depothead` VALUES ('165', '入库', '采购', 'CGRK00000000190', 'CGRK00000000190', '李佳忆', '2019-03-19 22:10:17', '2019-03-19 22:09:49', '47', null, '13', '-220.000000', '-220.000000', '现付', '', '', null, '', '0.000000', '0.000000', '220.000000', null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('166', '其它', '采购订单', 'CGDD00000000191', 'CGDD00000000191', '李佳忆', '2019-03-19 22:10:35', '2019-03-19 22:10:22', '50', null, null, '0.000000', '-2442.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('167', '出库', '采购退货', 'CGTH00000000193', 'CGTH00000000193', '李佳忆', '2019-03-19 22:11:39', '2019-03-19 22:11:12', '47', null, '13', '110.000000', '110.000000', '现付', '', '', null, '', '0.000000', '0.000000', '110.000000', null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('168', '其它', '销售订单', 'XSDD00000000194', 'XSDD00000000194', '李佳忆', '2019-03-19 22:12:04', '2019-03-19 22:11:55', '48', null, null, '0.000000', '22.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('169', '出库', '销售', 'XSCK00000000195', 'XSCK00000000195', '李佳忆', '2019-03-19 22:12:18', '2019-03-19 22:12:09', '48', null, '13', '22.000000', '22.000000', '现付', '', '', null, '', '0.000000', '0.000000', '22.000000', null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('170', '入库', '销售退货', 'XSTH00000000196', 'XSTH00000000196', '李佳忆', '2019-03-19 22:12:29', '2019-03-19 22:12:21', '48', null, '13', '-22.000000', '-22.000000', '现付', '', '', null, '', '0.000000', '0.000000', '22.000000', null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('171', '出库', '零售', 'LSCK00000000197', 'LSCK00000000197', '李佳忆', '2019-03-19 22:12:43', '2019-03-19 22:12:35', '49', null, '13', '22.000000', '22.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('172', '入库', '零售退货', 'LSTH00000000198', 'LSTH00000000198', '李佳忆', '2019-03-19 22:12:53', '2019-03-19 22:12:46', '49', null, '13', '-22.000000', '-22.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('173', '入库', '其它', 'QTRK00000000199', 'QTRK00000000199', '李佳忆', '2019-03-19 22:13:20', '2019-03-19 22:13:09', '50', null, null, '0.000000', '2200.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('174', '出库', '其它', 'QTCK00000000200', 'QTCK00000000200', '李佳忆', '2019-03-19 22:13:34', '2019-03-19 22:13:23', '48', null, null, '0.000000', '176.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '1', '0');
INSERT INTO `jsh_depothead` VALUES ('180', '入库', '采购', 'CGRK00000000242', 'CGRK00000000242', '李佳忆', '2019-04-02 22:30:01', '2019-04-02 22:29:52', '55', null, '16', '-1221.000000', '-1221.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1221.000000', null, null, null, null, '0', '', '115', '0');
INSERT INTO `jsh_depothead` VALUES ('181', '入库', '采购', 'CGRK00000000243', 'CGRK00000000243', '李佳忆', '2019-04-02 22:30:20', '2019-04-02 22:30:03', '55', null, '16', '-1342.000000', '-1342.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1342.000000', null, null, null, null, '0', '', '115', '0');
INSERT INTO `jsh_depothead` VALUES ('189', '入库', '采购', 'CGRK00000000261', 'CGRK00000000261', '李佳忆', '2019-04-10 22:25:49', '2020-02-20 23:51:03', '57', null, '17', '-120.000000', '-120.000000', '现付', '', '', null, '', '0.000000', '0.000000', '120.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('190', '入库', '采购', 'CGRK00000000263', 'CGRK00000000263', '李佳忆', '2019-04-13 19:57:43', '2019-04-13 19:57:32', '57', null, '17', '-24.000000', '-24.000000', '现付', '', '', null, '', '0.000000', '0.000000', '24.000000', null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('191', '入库', '采购', 'CGRK00000000264', 'CGRK00000000264', '李佳忆', '2019-04-13 19:57:58', '2020-02-20 23:50:55', '57', null, '17', '-10.000000', '-10.000000', '现付', '', '', null, '', '0.000000', '0.000000', '10.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('192', '入库', '采购', 'CGRK00000000265', 'CGRK00000000265', '李佳忆', '2019-04-20 00:36:24', '2020-02-20 23:50:47', '57', null, '17', '-220.000000', '-220.000000', '现付', '', '', null, '', '0.000000', '0.000000', '220.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('193', '出库', '销售', 'XSCK00000000268', 'XSCK00000000268', '李佳忆', '2019-04-29 23:41:02', '2020-02-20 23:52:17', '58', null, '17', '300.000000', '300.000000', '现付', '', '', null, '', '0.000000', '0.000000', '300.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('194', '入库', '采购', 'CGRK00000000272', 'CGRK00000000272', '李佳忆', '2019-04-30 22:33:24', '2020-02-20 23:50:28', '57', null, '17', '-1000.000000', '-1000.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1000.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('195', '入库', '采购', 'CGRK00000000273', 'CGRK00000000273', '李佳忆', '2019-04-30 22:34:45', '2020-02-20 23:49:49', '57', null, '17', '-1220.000000', '-1220.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1220.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('196', '入库', '采购', 'CGRK00000000274', 'CGRK00000000274', '李佳忆', '2019-04-30 22:35:53', '2020-02-20 23:49:07', '57', null, '18', '-1930.000000', '-1930.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1930.000000', '0.000000', '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('197', '出库', '销售', 'XSCK00000000290', 'XSCK00000000290', '李佳忆', '2019-04-30 23:15:27', '2020-02-20 23:52:01', '58', null, '17', '270.000000', '270.000000', '现付', '', '', null, '', '0.000000', '0.000000', '270.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('198', '入库', '采购', 'CGRK00000000292', 'CGRK00000000292', '李佳忆', '2019-05-03 14:20:56', '2019-05-03 14:19:38', '57', null, '17', '-1.120000', '-1.000000', '现付', '', '', null, '', '0.000000', '0.000000', '1.120000', null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('199', '其它', '采购订单', 'CGDD00000000305', 'CGDD00000000305', '李佳忆', '2019-12-28 12:16:36', '2020-02-20 23:47:56', '57', '63', null, '0.000000', '-11.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('200', '出库', '采购退货', 'CGTH00000000306', 'CGTH00000000306', '李佳忆', '2019-12-28 12:16:55', '2020-02-20 23:51:28', '57', '63', '17', '11.000000', '11.000000', '现付', '', '', null, '', '0.000000', '0.000000', '11.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('201', '其它', '销售订单', 'XSDD00000000307', 'XSDD00000000307', '李佳忆', '2019-12-28 12:17:09', '2020-02-20 23:51:37', '58', '63', null, '0.000000', '15.000000', '现付', '', '<14>', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('202', '入库', '销售退货', 'XSTH00000000308', 'XSTH00000000308', '李佳忆', '2019-12-28 12:17:22', '2020-02-20 23:52:33', '58', '63', '17', '-15.000000', '-15.000000', '现付', '', '', null, '', '0.000000', '0.000000', '15.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('203', '入库', '其它', 'QTRK00000000309', 'QTRK00000000309', '李佳忆', '2019-12-28 12:17:40', '2020-02-20 23:52:51', '57', '63', null, '0.000000', '21.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('204', '出库', '其它', 'QTCK00000000310', 'QTCK00000000310', '李佳忆', '2019-12-28 12:17:48', '2020-02-20 23:53:04', '58', '63', null, '0.000000', '15.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('205', '出库', '调拨', 'DBCK00000000311', 'DBCK00000000311', '李佳忆', '2019-12-28 12:17:58', '2020-02-20 23:53:21', null, '63', null, '0.000000', '15.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('206', '其它', '组装单', 'ZZD00000000312', 'ZZD00000000312', '李佳忆', '2019-12-28 12:18:09', '2020-02-20 23:54:02', null, '63', null, '0.000000', '0.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('207', '其它', '拆卸单', 'CXD00000000313', 'CXD00000000313', '李佳忆', '2019-12-28 12:18:47', '2020-02-20 23:54:21', null, '63', null, '0.000000', '0.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('208', '出库', '零售', 'LSCK00000000314', 'LSCK00000000314', '李佳忆', '2019-12-28 12:20:26', '2019-12-28 12:20:14', '60', '63', '17', '6.000000', '6.000000', '预付款', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('209', '入库', '零售退货', 'LSTH00000000315', 'LSTH00000000315', '李佳忆', '2019-12-28 12:20:39', '2019-12-28 12:20:29', '60', '63', '17', '-6.000000', '-6.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('210', '入库', '采购', 'CGRK00000000318', 'CGRK00000000318', '李佳忆', '2020-02-20 23:22:38', '2020-02-20 23:22:27', '57', '63', '17', '-110.000000', '-110.000000', '现付', '', '', null, '', '0.000000', '0.000000', '110.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('211', '入库', '采购', 'CGRK00000000319', 'CGRK00000000319', '李佳忆', '2020-02-20 23:54:48', '2020-02-20 23:54:33', '57', '63', '17', '-2400.000000', '-2400.000000', '现付', '', '', null, '', '0.000000', '0.000000', '2400.000000', null, '[\"undefined\"]', '[\"undefined\"]', null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('220', '其它', '采购订单', 'CGDD00000000355', 'CGDD00000000355', '李佳忆', '2020-07-07 20:09:03', '2020-07-07 20:08:54', '75', '133', null, '0.000000', '-1.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('232', '其它', '采购订单', 'CGDD00000000361', 'CGDD00000000361', '李佳忆', '2020-07-07 20:13:48', '2020-07-07 20:13:23', '57', '133', null, '0.000000', '-2.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('233', '其它', '采购订单', 'CGDD00000000362', 'CGDD00000000362', '李佳忆', '2020-07-07 20:14:12', '2020-07-07 20:13:59', '75', '133', null, '0.000000', '-2.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('234', '其它', '采购订单', 'CGDD00000000363', 'CGDD00000000363', '李佳忆', '2020-07-07 20:14:25', '2020-07-07 20:14:15', '75', '133', null, '0.000000', '-10.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('235', '入库', '采购', 'CGRK00000000364', 'CGRK00000000364', '李佳忆', '2020-07-07 20:14:44', '2020-07-07 20:14:34', '57', '133', '17', '-10.000000', '-10.000000', '现付', '', '', null, '', '0.000000', '0.000000', '10.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('239', '入库', '采购', 'CGRK00000000368', 'CGRK00000000368', '李佳忆', '2020-07-07 20:16:09', '2020-07-07 20:15:58', '75', '133', '17', '-2.000000', '-2.000000', '现付', '', '', null, '', '0.000000', '0.000000', '2.000000', null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('244', '入库', '零售退货', 'LSTH00000000370', 'LSTH00000000370', '李佳忆', '2020-07-07 20:18:11', '2020-07-07 20:17:05', '74', '133', '17', '-3.000000', '-3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('248', '入库', '零售退货', 'LSTH00000000371', 'LSTH00000000371', '李佳忆', '2020-07-07 20:19:26', '2020-07-07 20:19:15', '74', '133', '17', '-4.000000', '-4.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('250', '入库', '零售退货', 'LSTH00000000373', 'LSTH00000000373', '李佳忆', '2020-07-07 20:20:18', '2020-07-07 20:20:09', '74', '133', '17', '-20.000000', '-20.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('256', '其它', '采购订单', 'CGDD00000000380', 'CGDD00000000380', '李佳忆', '2020-07-08 09:42:41', '2020-07-08 09:41:33', '77', '133', null, '0.000000', '-3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('257', '其它', '采购订单', 'CGDD00000000381', 'CGDD00000000381', '李佳忆', '2020-07-08 09:43:42', '2020-07-08 09:42:50', '75', '133', null, '0.000000', '-1.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('258', '入库', '采购', 'CGRK00000000382', 'CGRK00000000382', '李佳忆', '2020-07-08 09:44:47', '2020-07-08 09:44:15', '77', '133', '17', '-3.000000', '-3.000000', '现付', '', '', null, '', '0.000000', '0.000000', '3.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('262', '入库', '采购', 'CGRK00000000385', 'CGRK00000000385', '李佳忆', '2020-07-08 09:46:38', '2020-07-08 09:46:28', '75', '133', '17', '-3.000000', '-3.000000', '现付', '', '', null, '', '0.000000', '0.000000', '3.000000', null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('263', '入库', '零售退货', 'LSTH00000000387', 'LSTH00000000387', '李佳忆', '2020-07-08 09:48:14', '2020-07-08 09:48:02', '60', '133', '17', '-3.000000', '-3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('265', '其它', '销售订单', 'XSDD00000000390', 'XSDD00000000390', '李佳忆', '2020-07-08 09:51:10', '2020-07-08 09:50:49', '1111', '133', null, '0.000000', '3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '1', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('269', '入库', '销售退货', 'XSTH00000000392', 'XSTH00000000392', '李佳忆', '2020-07-08 09:52:59', '2020-07-08 09:52:21', '111111', '133', '17', '-3.000000', '-3.000000', '现付', '', '', null, '', '0.000000', '0.000000', '3.000000', null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('270', '入库', '零售退货', 'LSTH00000000395', 'LSTH00000000395', '李佳忆', '2020-07-08 09:57:33', '2020-07-08 09:57:11', '60', '133', '17', '-1009.000000', '-1009.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('272', '入库', '采购', 'CGRK00000000398', 'CGRK00000000398', '李佳忆', '2020-07-08 10:00:02', '2020-07-08 09:59:12', '75', '133', '17', '-12.000000', '-12.000000', '现付', '', '', null, '', '0.000000', '0.000000', '12.000000', null, null, null, null, '0', '', '63', '1');
INSERT INTO `jsh_depothead` VALUES ('275', '入库', '其它', 'QTRK00000000401', 'QTRK00000000401', '李佳忆', '2020-07-08 10:06:52', '2020-07-08 10:06:40', '77', '133', null, '0.000000', '3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('277', '入库', '其它', 'QTRK00000000403', 'QTRK00000000403', '李佳忆', '2020-07-08 10:08:53', '2020-07-08 10:08:36', '75', '133', null, '0.000000', '3.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('280', '其它', '组装单', 'ZZD00000000406', 'ZZD00000000406', '李佳忆', '2020-07-08 10:10:11', '2020-07-08 10:09:59', null, '133', null, '0.000000', '0.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');
INSERT INTO `jsh_depothead` VALUES ('281', '其它', '拆卸单', 'CXD00000000407', 'CXD00000000407', '李佳忆', '2020-07-08 10:10:44', '2020-07-08 10:10:34', null, '133', null, '0.000000', '0.000000', '现付', '', '', null, '', null, null, null, null, null, null, null, '0', '', '63', '0');

-- ----------------------------
-- Table structure for `jsh_depotitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_depotitem`;
CREATE TABLE `jsh_depotitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `HeaderId` bigint(20) NOT NULL COMMENT '表头Id',
  `MaterialId` bigint(20) NOT NULL COMMENT '材料Id',
  `material_extend_id` bigint(20) DEFAULT NULL COMMENT '商品扩展id',
  `MUnit` varchar(20) DEFAULT NULL COMMENT '商品计量单位',
  `OperNumber` decimal(24,6) DEFAULT NULL COMMENT '数量',
  `BasicNumber` decimal(24,6) DEFAULT NULL COMMENT '基础数量，如kg、瓶',
  `UnitPrice` decimal(24,6) DEFAULT NULL COMMENT '单价',
  `TaxUnitPrice` decimal(24,6) DEFAULT NULL COMMENT '含税单价',
  `AllPrice` decimal(24,6) DEFAULT NULL COMMENT '金额',
  `Remark` varchar(200) DEFAULT NULL COMMENT '描述',
  `Img` varchar(50) DEFAULT NULL COMMENT '图片',
  `Incidentals` decimal(24,6) DEFAULT NULL COMMENT '运杂费',
  `DepotId` bigint(20) DEFAULT NULL COMMENT '仓库ID（库存是统计出来的）',
  `AnotherDepotId` bigint(20) DEFAULT NULL COMMENT '调拨时，对方仓库Id',
  `TaxRate` decimal(24,6) DEFAULT NULL COMMENT '税率',
  `TaxMoney` decimal(24,6) DEFAULT NULL COMMENT '税额',
  `TaxLastMoney` decimal(24,6) DEFAULT NULL COMMENT '价税合计',
  `OtherField1` varchar(50) DEFAULT NULL COMMENT '自定义字段1-品名',
  `OtherField2` varchar(50) DEFAULT NULL COMMENT '自定义字段2-型号',
  `OtherField3` varchar(50) DEFAULT NULL COMMENT '自定义字段3-制造商',
  `OtherField4` varchar(50) DEFAULT NULL COMMENT '自定义字段4',
  `OtherField5` varchar(50) DEFAULT NULL COMMENT '自定义字段5',
  `MType` varchar(20) DEFAULT NULL COMMENT '商品类型',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`),
  KEY `FK2A819F475D61CCF7` (`MaterialId`),
  KEY `FK2A819F474BB6190E` (`HeaderId`),
  KEY `FK2A819F479485B3F5` (`DepotId`),
  KEY `FK2A819F47729F5392` (`AnotherDepotId`)
) ENGINE=InnoDB AUTO_INCREMENT=254 DEFAULT CHARSET=utf8 COMMENT='单据子表';

-- ----------------------------
-- Records of jsh_depotitem
-- ----------------------------
INSERT INTO `jsh_depotitem` VALUES ('172', '165', '564', null, '个', '10.000000', '10.000000', '22.000000', '22.000000', '220.000000', '', null, null, '7', null, '0.000000', '0.000000', '220.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('173', '166', '564', null, '个', '111.000000', '111.000000', '22.000000', '22.000000', '2442.000000', '', null, null, '7', null, '0.000000', '0.000000', '2442.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('174', '167', '564', null, '个', '5.000000', '5.000000', '22.000000', '22.000000', '110.000000', '', null, null, '7', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('175', '168', '564', null, '个', '1.000000', '1.000000', '22.000000', '22.000000', '22.000000', '', null, null, '7', null, '0.000000', '0.000000', '22.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('176', '169', '564', null, '个', '1.000000', '1.000000', '22.000000', '22.000000', '22.000000', '', null, null, '7', null, '0.000000', '0.000000', '22.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('177', '170', '564', null, '个', '1.000000', '1.000000', '22.000000', '22.000000', '22.000000', '', null, null, '7', null, '0.000000', '0.000000', '22.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('178', '171', '564', null, '个', '1.000000', '1.000000', '22.000000', '22.000000', '22.000000', '', null, null, '7', null, '0.000000', '0.000000', '22.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('179', '172', '564', null, '个', '1.000000', '1.000000', '22.000000', '22.000000', '22.000000', '', null, null, '7', null, '0.000000', '0.000000', '22.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('180', '173', '564', null, '个', '100.000000', '100.000000', '22.000000', '22.000000', '2200.000000', '', null, null, '7', null, '0.000000', '0.000000', '2200.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('181', '174', '564', null, '个', '8.000000', '8.000000', '22.000000', '22.000000', '176.000000', '', null, null, '7', null, '0.000000', '0.000000', '176.000000', '', '', '', '', '', '', '1', '0');
INSERT INTO `jsh_depotitem` VALUES ('187', '180', '567', null, '个', '111.000000', null, '11.000000', '11.000000', '1221.000000', '', null, null, '13', null, '0.000000', '0.000000', '1221.000000', '', '', '', '', '', '', '115', '0');
INSERT INTO `jsh_depotitem` VALUES ('188', '181', '567', null, '个', '122.000000', null, '11.000000', '11.000000', '1342.000000', '', null, null, '13', null, '0.000000', '0.000000', '1342.000000', '', '', '', '', '', '', '115', '0');
INSERT INTO `jsh_depotitem` VALUES ('198', '189', '569', '3', '只', '12.000000', '12.000000', '10.000000', '10.000000', '120.000000', '', null, null, '14', null, '0.000000', '0.000000', '120.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('199', '190', '569', null, '只', '12.000000', '12.000000', '2.000000', '2.000000', '24.000000', '', null, null, '14', null, '0.000000', '0.000000', '24.000000', '', '', '', '', '', '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('200', '191', '569', '3', '只', '1.000000', '1.000000', '10.000000', '10.000000', '10.000000', '', null, null, '14', null, '0.000000', '0.000000', '10.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('201', '192', '569', '3', '只', '22.000000', '22.000000', '10.000000', '10.000000', '220.000000', '', null, null, '14', null, '0.000000', '0.000000', '220.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('202', '193', '569', '3', '只', '20.000000', '0.000000', '15.000000', '15.000000', '300.000000', '', null, null, '14', null, '0.000000', '0.000000', '300.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('203', '194', '569', '3', '只', '100.000000', '100.000000', '10.000000', '10.000000', '1000.000000', '', null, null, '15', null, '0.000000', '0.000000', '1000.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('204', '195', '569', '3', '只', '122.000000', '122.000000', '10.000000', '10.000000', '1220.000000', '', null, null, '15', null, '0.000000', '0.000000', '1220.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('205', '196', '569', '3', '只', '2.000000', '122.000000', '10.000000', '10.000000', '20.000000', '', null, null, '15', null, '0.000000', '0.000000', '20.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('206', '197', '569', '3', '只', '18.000000', '0.000000', '15.000000', '15.000000', '270.000000', '', null, null, '14', null, '0.000000', '0.000000', '270.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('207', '196', '568', '2', '个', '10.000000', '2.000000', '11.000000', '11.000000', '110.000000', '', null, null, '15', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('208', '196', '568', '2', '个', '10.000000', '2.000000', '11.000000', '11.000000', '110.000000', '', null, null, '15', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('209', '196', '568', '2', '个', '10.000000', '2.000000', '11.000000', '11.000000', '110.000000', '', null, null, '14', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('210', '196', '568', '2', '个', '10.000000', '2.000000', '11.000000', '11.000000', '110.000000', '', null, null, '14', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('211', '196', '568', '2', '个', '10.000000', '3.000000', '11.000000', '11.000000', '110.000000', '', null, null, '14', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('212', '196', '568', '2', '个', '10.000000', '4.000000', '11.000000', '11.000000', '110.000000', '', null, null, '14', null, '0.000000', '0.000000', '110.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('213', '196', '568', '2', '个', '100.000000', '5.000000', '11.000000', '11.000000', '1100.000000', '', null, null, '14', null, '0.000000', '0.000000', '1100.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('214', '196', '569', '3', '只', '15.000000', '6.000000', '10.000000', '10.000000', '150.000000', '', null, null, '14', null, '0.000000', '0.000000', '150.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('215', '198', '578', null, '箱', '1.000000', '12.000000', '1.000000', '1.120000', '1.000000', '', null, null, '14', null, '12.000000', '0.120000', '1.120000', '', '', '', '', '', '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('216', '199', '568', '2', '个', '1.000000', '1.000000', '11.000000', '11.000000', '11.000000', '', null, null, '14', null, '0.000000', '0.000000', '11.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('217', '200', '568', '2', '个', '1.000000', '0.000000', '11.000000', '11.000000', '11.000000', '', null, null, '14', null, '0.000000', '0.000000', '11.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('218', '201', '568', '2', '个', '1.000000', '1.000000', '15.000000', '15.000000', '15.000000', '', null, null, '14', null, '0.000000', '0.000000', '15.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('219', '202', '568', '2', '个', '1.000000', '1.000000', '15.000000', '15.000000', '15.000000', '', null, null, '14', null, '0.000000', '0.000000', '15.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('220', '203', '568', '2', '个', '1.000000', '1.000000', '11.000000', '11.000000', '11.000000', '', null, null, '14', null, '0.000000', '0.000000', '11.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('221', '203', '569', '3', '只', '1.000000', '1.000000', '10.000000', '10.000000', '10.000000', '', null, null, '14', null, '0.000000', '0.000000', '10.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('222', '204', '569', '3', '只', '1.000000', '0.000000', '15.000000', '15.000000', '15.000000', '', null, null, '14', null, '0.000000', '0.000000', '15.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('223', '205', '568', '2', '个', '1.000000', '1.000000', '15.000000', '15.000000', '15.000000', '', null, null, '14', '15', '0.000000', '0.000000', '15.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('224', '206', '568', '2', '个', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '', null, null, '14', null, '0.000000', '0.000000', '0.000000', '', '', '', '', '', '组合件', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('225', '206', '569', '3', '只', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '', null, null, '14', null, '0.000000', '0.000000', '0.000000', '', '', '', '', '', '普通子件', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('226', '207', '569', '3', '只', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '', null, null, '14', null, '0.000000', '0.000000', '0.000000', '', '', '', '', '', '组合件', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('227', '207', '568', '2', '个', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '', null, null, '14', null, '0.000000', '0.000000', '0.000000', '', '', '', '', '', '普通子件', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('228', '208', '568', null, '个', '1.000000', '1.000000', '6.000000', '6.000000', '6.000000', '', null, null, '14', null, '0.000000', '0.000000', '6.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('229', '209', '568', null, '个', '1.000000', '1.000000', '6.000000', '6.000000', '6.000000', '', null, null, '14', null, '0.000000', '0.000000', '6.000000', '', '', '', '', '', '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('230', '210', '587', '1', '个', '10.000000', '10.000000', '11.000000', '11.000000', '110.000000', '', null, null, '14', null, '0.000000', '0.000000', '110.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('231', '211', '579', '8', '箱', '10.000000', '120.000000', '240.000000', '240.000000', '2400.000000', '', null, null, '14', null, '0.000000', '0.000000', '2400.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('232', '220', '590', '12', '1', '1.000000', '1.000000', '1.000000', '1.000000', '1.000000', '', null, null, '1', null, '0.000000', '0.000000', '1.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('233', '232', '591', '13', '2', '1.000000', '1.000000', '2.000000', '2.000000', '2.000000', '2', null, null, '2', null, '0.000000', '0.000000', '2.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('234', '233', '591', '13', '2', '1.000000', '1.000000', '2.000000', '2.000000', '2.000000', '2', null, null, '1', null, '0.000000', '0.000000', '2.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('235', '234', '569', '3', '只', '1.000000', '1.000000', '10.000000', '10.000000', '10.000000', '1', null, null, '1', null, '0.000000', '0.000000', '10.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('236', '235', '569', '3', '只', '1.000000', '1.000000', '10.000000', '10.000000', '10.000000', '1', null, null, '1', null, '0.000000', '0.000000', '10.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('237', '239', '591', '13', '2', '1.000000', '1.000000', '2.000000', '2.000000', '2.000000', '1', null, null, '1', null, '0.000000', '0.000000', '2.000000', null, null, null, null, null, '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('238', '244', '592', '14', '3', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '1', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('239', '248', '593', '15', '4', '1.000000', '1.000000', '4.000000', '4.000000', '4.000000', '1', null, null, '1', null, '0.000000', '0.000000', '4.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('240', '250', '577', '5', '个', '1.000000', '1.000000', '20.000000', '20.000000', '20.000000', '1', null, null, '1', null, '0.000000', '0.000000', '20.000000', null, null, null, null, null, '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('241', '256', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '1111', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('242', '257', '594', '16', '把', '1.000000', '1.000000', '1.000000', '1.000000', '1.000000', '1111', null, null, '1', null, '0.000000', '0.000000', '1.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('243', '258', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('244', '262', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('245', '263', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '1', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('246', '265', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('247', '269', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('248', '270', '588', '10', '1009', '1.000000', '1.000000', '1009.000000', '1009.000000', '1009.000000', '111', null, null, '1', null, '0.000000', '0.000000', '1009.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('249', '272', '586', '9', '个', '1.000000', '1.000000', '12.000000', '12.000000', '12.000000', '无', null, null, '1', null, '0.000000', '0.000000', '12.000000', null, null, null, null, null, '', '63', '1');
INSERT INTO `jsh_depotitem` VALUES ('250', '275', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('251', '277', '595', '17', '个', '1.000000', '1.000000', '3.000000', '3.000000', '3.000000', '', null, null, '1', null, '0.000000', '0.000000', '3.000000', null, null, null, null, null, '', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('252', '280', '594', '16', '把', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '1', null, null, '1', null, '0.000000', '0.000000', '0.000000', null, null, null, null, null, '组合件', '63', '0');
INSERT INTO `jsh_depotitem` VALUES ('253', '281', '594', '16', '把', '1.000000', '1.000000', '0.000000', '0.000000', '0.000000', '1', null, null, '1', null, '0.000000', '0.000000', '0.000000', null, null, null, null, null, '组合件', '63', '0');

-- ----------------------------
-- Table structure for `jsh_functions`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_functions`;
CREATE TABLE `jsh_functions` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Number` varchar(50) DEFAULT NULL COMMENT '编号',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `PNumber` varchar(50) DEFAULT NULL COMMENT '上级编号',
  `URL` varchar(100) DEFAULT NULL COMMENT '链接',
  `State` bit(1) DEFAULT NULL COMMENT '收缩',
  `Sort` varchar(50) DEFAULT NULL COMMENT '排序',
  `Enabled` bit(1) DEFAULT NULL COMMENT '启用',
  `Type` varchar(50) DEFAULT NULL COMMENT '类型',
  `PushBtn` varchar(50) DEFAULT NULL COMMENT '功能按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '图标',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8 COMMENT='功能模块表';

-- ----------------------------
-- Records of jsh_functions
-- ----------------------------
INSERT INTO `jsh_functions` VALUES ('1', '0001', '系统管理', '0', '', '', '0910', '', '电脑版', '', 'icon-settings', '0');
INSERT INTO `jsh_functions` VALUES ('13', '000102', '角色管理', '0001', '/pages/manage/role.html', '\0', '0130', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('14', '000103', '用户管理', '0001', '/pages/manage/user.html', '\0', '0140', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('15', '000104', '日志管理', '0001', '/pages/manage/log.html', '\0', '0160', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('16', '000105', '功能管理', '0001', '/pages/manage/functions.html', '\0', '0135', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('21', '0101', '物资管理', '0', '', '\0', '0620', '', '电脑版', null, 'icon-social-dropbox', '0');
INSERT INTO `jsh_functions` VALUES ('22', '010101', '物资类别', '0101', '/pages/materials/materialcategory.html', '\0', '0230', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('23', '010102', '物资信息', '0101', '/pages/materials/material.html', '\0', '0240', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('24', '0102', '基本资料', '0', '', '\0', '0750', '', '电脑版', null, 'icon-grid', '0');
INSERT INTO `jsh_functions` VALUES ('25', '01020101', '供应商信息', '0102', '/pages/manage/vendor.html', '\0', '0260', '', '电脑版', '1,2', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('26', '010202', '仓库信息', '0102', '/pages/manage/depot.html', '\0', '0270', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('31', '010206', '经手人管理', '0102', '/pages/materials/person.html', '\0', '0284', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('32', '0502', '采购管理', '0', '', '\0', '0330', '', '电脑版', '', 'icon-loop', '0');
INSERT INTO `jsh_functions` VALUES ('33', '050201', '采购入库', '0502', '/pages/materials/purchase_in_list.html', '\0', '0340', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('38', '0603', '销售管理', '0', '', '\0', '0390', '', '电脑版', '', 'icon-briefcase', '0');
INSERT INTO `jsh_functions` VALUES ('40', '080107', '调拨出库', '0801', '/pages/materials/allocation_out_list.html', '\0', '0807', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('41', '060303', '销售出库', '0603', '/pages/materials/sale_out_list.html', '\0', '0394', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('44', '0704', '财务管理', '0', '', '\0', '0450', '', '电脑版', '', 'icon-map', '0');
INSERT INTO `jsh_functions` VALUES ('59', '030101', '库存状况', '0301', '/pages/reports/in_out_stock_report.html', '\0', '0600', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('194', '010204', '收支项目', '0102', '/pages/manage/inOutItem.html', '\0', '0282', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('195', '010205', '结算账户', '0102', '/pages/manage/account.html', '\0', '0283', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('197', '070402', '收入单', '0704', '/pages/financial/item_in.html', '\0', '0465', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('198', '0301', '报表查询', '0', '', '\0', '0570', '', '电脑版', null, 'icon-pie-chart', '0');
INSERT INTO `jsh_functions` VALUES ('199', '050204', '采购退货', '0502', '/pages/materials/purchase_back_list.html', '\0', '0345', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('200', '060305', '销售退货', '0603', '/pages/materials/sale_back_list.html', '\0', '0396', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('201', '080103', '其它入库', '0801', '/pages/materials/other_in_list.html', '\0', '0803', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('202', '080105', '其它出库', '0801', '/pages/materials/other_out_list.html', '\0', '0805', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('203', '070403', '支出单', '0704', '/pages/financial/item_out.html', '\0', '0470', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('204', '070404', '收款单', '0704', '/pages/financial/money_in.html', '\0', '0475', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('205', '070405', '付款单', '0704', '/pages/financial/money_out.html', '\0', '0480', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('206', '070406', '转账单', '0704', '/pages/financial/giro.html', '\0', '0490', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('207', '030102', '结算账户', '0301', '/pages/reports/account_report.html', '\0', '0610', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('208', '030103', '进货统计', '0301', '/pages/reports/buy_in_report.html', '\0', '0620', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('209', '030104', '销售统计', '0301', '/pages/reports/sale_out_report.html', '\0', '0630', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('210', '040102', '零售出库', '0401', '/pages/materials/retail_out_list.html', '\0', '0405', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('211', '040104', '零售退货', '0401', '/pages/materials/retail_back_list.html', '\0', '0407', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('212', '070407', '收预付款', '0704', '/pages/financial/advance_in.html', '\0', '0495', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('217', '01020102', '客户信息', '0102', '/pages/manage/customer.html', '\0', '0262', '', '电脑版', '1,2', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('218', '01020103', '会员信息', '0102', '/pages/manage/member.html', '\0', '0263', '', '电脑版', '1,2', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('220', '010103', '计量单位', '0101', '/pages/manage/unit.html', '\0', '0245', '', '电脑版', null, 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('225', '0401', '零售管理', '0', '', '\0', '0101', '', '电脑版', '', 'icon-present', '0');
INSERT INTO `jsh_functions` VALUES ('226', '030106', '入库明细', '0301', '/pages/reports/in_detail.html', '\0', '0640', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('227', '030107', '出库明细', '0301', '/pages/reports/out_detail.html', '\0', '0645', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('228', '030108', '入库汇总', '0301', '/pages/reports/in_material_count.html', '\0', '0650', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('229', '030109', '出库汇总', '0301', '/pages/reports/out_material_count.html', '\0', '0655', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('232', '080109', '组装单', '0801', '/pages/materials/assemble_list.html', '\0', '0809', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('233', '080111', '拆卸单', '0801', '/pages/materials/disassemble_list.html', '\0', '0811', '', '电脑版', '3,4,5', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('234', '000105', '系统配置', '0001', '/pages/manage/systemConfig.html', '\0', '0165', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('235', '030110', '客户对账', '0301', '/pages/reports/customer_account.html', '\0', '0660', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('236', '000106', '商品属性', '0001', '/pages/materials/materialProperty.html', '\0', '0168', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('237', '030111', '供应商对账', '0301', '/pages/reports/vendor_account.html', '\0', '0665', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('239', '0801', '仓库管理', '0', '', '\0', '0420', '', '电脑版', '', 'icon-layers', '0');
INSERT INTO `jsh_functions` VALUES ('240', '010104', '序列号', '0101', '/pages/manage/serialNumber.html', '\0', '0246', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('241', '050202', '采购订单', '0502', '/pages/materials/purchase_orders_list.html', '\0', '0335', '', '电脑版', '3', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('242', '060301', '销售订单', '0603', '/pages/materials/sale_orders_list.html', '\0', '0392', '', '电脑版', '3', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('243', '000108', '连队管理', '0001', '/pages/manage/organization.html', '', '0139', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('244', '030112', '库存预警', '0301', '/pages/reports/stock_warning_report.html', '\0', '0670', '', '电脑版', '', 'icon-notebook', '0');
INSERT INTO `jsh_functions` VALUES ('245', '000107', '插件管理', '0001', '/pages/manage/plugin.html', '\0', '0170', '', '电脑版', '', 'icon-notebook', '0');

-- ----------------------------
-- Table structure for `jsh_inoutitem`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_inoutitem`;
CREATE TABLE `jsh_inoutitem` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Type` varchar(20) DEFAULT NULL COMMENT '类型',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='收支项目';

-- ----------------------------
-- Records of jsh_inoutitem
-- ----------------------------
INSERT INTO `jsh_inoutitem` VALUES ('1', '办公耗材', '支出', '办公耗材', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('5', '房租收入', '收入', '房租收入', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('7', '利息收入', '收入', '利息收入', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('8', '水电费', '支出', '水电费水电费', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('9', '快递费', '支出', '快递费', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('10', '交通报销费', '支出', '交通报销费', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('11', '差旅费', '支出', '差旅费', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('12', '全车贴膜-普通', '收入', '', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('13', '全车贴膜-高档', '收入', '', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('14', '洗车', '收入', '', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('15', '保养汽车', '收入', '', null, '0');
INSERT INTO `jsh_inoutitem` VALUES ('16', '收入项目1', '收入', '', '1', '0');
INSERT INTO `jsh_inoutitem` VALUES ('17', '支出项目1', '支出', '', '1', '0');
INSERT INTO `jsh_inoutitem` VALUES ('18', '收入1', '收入', '', '117', '0');
INSERT INTO `jsh_inoutitem` VALUES ('19', '支出1', '支出', '', '117', '0');
INSERT INTO `jsh_inoutitem` VALUES ('20', '支出2', '支出', '', '117', '0');
INSERT INTO `jsh_inoutitem` VALUES ('21', '支出1', '支出', '', '63', '0');
INSERT INTO `jsh_inoutitem` VALUES ('22', '收入1', '收入', '', '63', '0');
INSERT INTO `jsh_inoutitem` VALUES ('23', '支出2', '支出', '', '63', '0');

-- ----------------------------
-- Table structure for `jsh_log`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_log`;
CREATE TABLE `jsh_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userID` bigint(20) NOT NULL COMMENT '操作用户ID',
  `operation` varchar(500) DEFAULT NULL COMMENT '操作模块名称',
  `clientIP` varchar(50) DEFAULT NULL COMMENT '客户端IP',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `status` tinyint(4) DEFAULT NULL COMMENT '操作状态 0==成功，1==失败',
  `contentdetails` varchar(1000) DEFAULT NULL COMMENT '操作详情',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注信息',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`),
  KEY `FKF2696AA13E226853` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=7017 DEFAULT CHARSET=utf8 COMMENT='操作日志';

-- ----------------------------
-- Records of jsh_log
-- ----------------------------
INSERT INTO `jsh_log` VALUES ('6441', '63', '商品', '127.0.0.1', '2020-02-20 23:47:04', '0', '修改,id:579商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6442', '63', '商品价格扩展', '127.0.0.1', '2020-02-20 23:47:23', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6443', '63', '商品价格扩展', '127.0.0.1', '2020-02-20 23:47:23', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6444', '63', '商品', '127.0.0.1', '2020-02-20 23:47:23', '0', '修改,id:586商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6445', '63', '单据', '127.0.0.1', '2020-02-20 23:47:56', '0', '修改,id:199单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6446', '63', '单据明细', '127.0.0.1', '2020-02-20 23:47:56', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6447', '63', '单据明细', '127.0.0.1', '2020-02-20 23:47:56', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6448', '63', '单据', '127.0.0.1', '2020-02-20 23:49:07', '0', '修改,id:196单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6449', '63', '单据明细', '127.0.0.1', '2020-02-20 23:49:07', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6450', '63', '单据明细', '127.0.0.1', '2020-02-20 23:49:07', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6451', '63', '单据', '127.0.0.1', '2020-02-20 23:49:49', '0', '修改,id:195单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6452', '63', '单据明细', '127.0.0.1', '2020-02-20 23:49:49', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6453', '63', '单据明细', '127.0.0.1', '2020-02-20 23:49:49', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6454', '63', '单据', '127.0.0.1', '2020-02-20 23:50:28', '0', '修改,id:194单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6455', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:28', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6456', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:28', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6457', '63', '单据', '127.0.0.1', '2020-02-20 23:50:47', '0', '修改,id:192单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6458', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:47', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6459', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:47', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6460', '63', '单据', '127.0.0.1', '2020-02-20 23:50:55', '0', '修改,id:191单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6461', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:55', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6462', '63', '单据明细', '127.0.0.1', '2020-02-20 23:50:55', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6463', '63', '单据', '127.0.0.1', '2020-02-20 23:51:03', '0', '修改,id:189单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6464', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:03', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6465', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:03', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6466', '63', '单据', '127.0.0.1', '2020-02-20 23:51:28', '0', '修改,id:200单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6467', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:28', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6468', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:28', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6469', '63', '单据', '127.0.0.1', '2020-02-20 23:51:37', '0', '修改,id:201单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6470', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:37', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6471', '63', '单据明细', '127.0.0.1', '2020-02-20 23:51:37', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6472', '63', '单据', '127.0.0.1', '2020-02-20 23:52:01', '0', '修改,id:197单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6473', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:01', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6474', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:01', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6475', '63', '单据', '127.0.0.1', '2020-02-20 23:52:17', '0', '修改,id:193单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6476', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:17', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6477', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:17', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6478', '63', '单据', '127.0.0.1', '2020-02-20 23:52:33', '0', '修改,id:202单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6479', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:33', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6480', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:33', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6481', '63', '单据', '127.0.0.1', '2020-02-20 23:52:51', '0', '修改,id:203单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6482', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:51', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6483', '63', '单据明细', '127.0.0.1', '2020-02-20 23:52:51', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6484', '63', '单据', '127.0.0.1', '2020-02-20 23:53:04', '0', '修改,id:204单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6485', '63', '单据明细', '127.0.0.1', '2020-02-20 23:53:04', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6486', '63', '单据明细', '127.0.0.1', '2020-02-20 23:53:04', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6487', '63', '单据', '127.0.0.1', '2020-02-20 23:53:21', '0', '修改,id:205单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6488', '63', '单据明细', '127.0.0.1', '2020-02-20 23:53:21', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6489', '63', '单据明细', '127.0.0.1', '2020-02-20 23:53:21', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6493', '63', '单据', '127.0.0.1', '2020-02-20 23:54:02', '0', '修改,id:206单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6494', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:02', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6495', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:02', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6496', '63', '单据', '127.0.0.1', '2020-02-20 23:54:21', '0', '修改,id:207单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6497', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:21', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6498', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:21', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6499', '63', '单据', '127.0.0.1', '2020-02-20 23:54:48', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6500', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:48', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6501', '63', '单据明细', '127.0.0.1', '2020-02-20 23:54:48', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6502', '120', '用户', '127.0.0.1', '2020-02-21 00:03:55', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6503', '63', '用户', '127.0.0.1', '2020-02-21 00:04:38', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6504', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 13:49:22', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6505', '120', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 14:37:31', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6506', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 14:48:49', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6507', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:41', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6508', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6509', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6510', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6511', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6512', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6513', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:42', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6514', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:43', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6515', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:43', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6516', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:43', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6517', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:44', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6518', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:44', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6519', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-24 15:07:46', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6520', '63', '系统配置', '0:0:0:0:0:0:0:1', '2020-04-24 15:21:51', '0', '修改,id:9系统配置', null, '63');
INSERT INTO `jsh_log` VALUES ('6521', '63', '单据', '0:0:0:0:0:0:0:1', '2020-04-24 15:23:16', '0', '修改,id:199单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6522', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 00:39:31', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6523', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 00:40:12', '0', '新增用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6524', '132', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 00:40:39', '0', '登录,id:132用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6525', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 00:42:01', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6526', '120', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 02:50:43', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6527', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 02:52:21', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6528', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 02:53:31', '0', '删除,id:131用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6529', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 02:53:35', '0', '删除,id:132用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6530', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:17:39', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6531', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:34:35', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6532', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:34:35', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6533', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:34:35', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6534', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:34:35', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6535', '63', '用户', '0:0:0:0:0:0:0:1', '2020-04-25 07:34:35', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6536', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:23:18', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6537', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:26:11', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6538', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:27:06', '0', '新增用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6539', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:27:24', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6540', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:28:48', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6541', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:31:17', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6542', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:33:02', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6543', '132', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:49:36', '0', '登录,id:132用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6544', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:51:22', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6545', '120', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-07 07:52:22', '0', '新增关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6546', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:52:41', '0', '修改,id:133用户', null, null);
INSERT INTO `jsh_log` VALUES ('6547', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 07:52:54', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6548', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 08:40:44', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6549', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 08:48:55', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6550', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 09:12:00', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6551', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 10:38:39', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6552', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 10:41:23', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6553', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:06:45', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6554', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:13:48', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6555', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:14:38', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6556', '133', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-07 13:22:17', '0', '新增关联关系', null, '63');
INSERT INTO `jsh_log` VALUES ('6557', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:22:45', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6558', '120', '角色', '0:0:0:0:0:0:0:1', '2020-07-07 13:23:44', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6559', '120', '角色', '0:0:0:0:0:0:0:1', '2020-07-07 13:23:50', '0', '修改,id:17角色', null, null);
INSERT INTO `jsh_log` VALUES ('6560', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:25:26', '0', '删除,id:121用户', null, null);
INSERT INTO `jsh_log` VALUES ('6561', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:28:30', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6562', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:29:33', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6563', '120', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-07 13:31:38', '0', '修改,id:38关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6564', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:32:07', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6565', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:32:52', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6566', '120', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 13:34:11', '0', '删除,id:15序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6567', '120', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 13:34:16', '0', '删除,id:17序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6568', '120', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 13:34:22', '0', '删除,id:16序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6569', '120', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 13:34:26', '0', '删除,id:13序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6570', '120', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 13:34:37', '0', '删除,id:12序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6571', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 13:37:57', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6572', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-07 13:46:46', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6573', '133', '仓库', '0:0:0:0:0:0:0:1', '2020-07-07 13:54:08', '0', '新增仓库', null, '63');
INSERT INTO `jsh_log` VALUES ('6574', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 13:57:33', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6575', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 13:57:34', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6576', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 13:57:34', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6577', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-07 14:03:08', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6578', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 14:05:26', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6579', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 14:05:27', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6580', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 14:05:27', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6581', '133', '计量单位', '0:0:0:0:0:0:0:1', '2020-07-07 14:05:48', '0', '新增计量单位', null, '63');
INSERT INTO `jsh_log` VALUES ('6582', '133', '计量单位', '0:0:0:0:0:0:0:1', '2020-07-07 14:05:53', '0', '删除,id:16计量单位', null, '63');
INSERT INTO `jsh_log` VALUES ('6583', '133', '商品类型', '0:0:0:0:0:0:0:1', '2020-07-07 14:08:21', '0', '新增商品类型', null, '63');
INSERT INTO `jsh_log` VALUES ('6584', '133', '商品类型', '0:0:0:0:0:0:0:1', '2020-07-07 14:08:34', '0', '删除,id:18商品类型', null, '63');
INSERT INTO `jsh_log` VALUES ('6585', '133', '序列号', '0:0:0:0:0:0:0:1', '2020-07-07 14:12:23', '0', '新增序列号', null, '63');
INSERT INTO `jsh_log` VALUES ('6599', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-07 14:31:21', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6600', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-07 14:31:50', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6601', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:06:03', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6602', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:06:03', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6603', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 20:06:03', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6613', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:09:03', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6614', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:09:03', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6615', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:09:03', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6649', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:12:48', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6650', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:12:48', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6651', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 20:12:48', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6652', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:13:48', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6653', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:13:48', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6654', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:13:48', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6655', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:12', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6656', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:12', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6657', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:12', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6658', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:25', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6659', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:25', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6660', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:25', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6661', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:44', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6662', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:44', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6663', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:14:44', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6673', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:16:09', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6674', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:16:09', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6675', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:16:09', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6690', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:17:42', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6691', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:17:42', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6692', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 20:17:42', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6693', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:11', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6694', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:11', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6695', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:11', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6700', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:48', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6701', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:48', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6702', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-07 20:18:48', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6710', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:19:26', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6711', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:19:26', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6712', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:19:26', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6716', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-07 20:20:18', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6717', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:20:18', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6718', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-07 20:20:18', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6719', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 09:10:52', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6720', '133', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 09:19:00', '0', '新增机构', null, '63');
INSERT INTO `jsh_log` VALUES ('6721', '133', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 09:19:19', '0', '修改,id:13机构', null, '63');
INSERT INTO `jsh_log` VALUES ('6722', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-08 09:22:52', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6723', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-08 09:22:52', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6724', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-08 09:22:52', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6725', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:23:38', '0', '修改,id:220单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6726', '133', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 09:24:28', '0', '新增仓库', null, '63');
INSERT INTO `jsh_log` VALUES ('6727', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 09:27:11', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6728', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 09:29:04', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6729', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 09:31:08', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6730', '133', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 09:33:23', '0', '新增仓库', null, '63');
INSERT INTO `jsh_log` VALUES ('6731', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-08 09:34:09', '0', '新增商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6732', '133', '商品价格扩展', '0:0:0:0:0:0:0:1', '2020-07-08 09:34:09', '0', '删除,id:商品价格扩展', null, '63');
INSERT INTO `jsh_log` VALUES ('6733', '133', '商品', '0:0:0:0:0:0:0:1', '2020-07-08 09:34:09', '0', '新增商品', null, '63');
INSERT INTO `jsh_log` VALUES ('6737', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 09:39:23', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6750', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:42:41', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6751', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:42:41', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6752', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:42:41', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6753', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:43:42', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6754', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:43:42', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6755', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:43:42', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6756', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:43:50', '0', '修改,id:234单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6757', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:44:11', '0', '修改,id:234单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6758', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:44:47', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6759', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:44:47', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6760', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:44:47', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6770', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:46:38', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6771', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:46:38', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6772', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:46:38', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6773', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:48:14', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6774', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:48:14', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6775', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:48:14', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6780', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:04', '0', '删除,id:263,250单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6781', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:04', '0', '删除,id:263单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6782', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:04', '0', '删除,id:263单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6783', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:04', '0', '删除,id:250单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6784', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:04', '0', '删除,id:250单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6785', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 09:49:58', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6786', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:51:10', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6787', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:51:10', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6788', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:51:10', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6798', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:52:59', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6799', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:52:59', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6800', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:52:59', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6801', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 09:56:08', '0', '登录,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6802', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 09:57:33', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6803', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:57:33', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6804', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 09:57:33', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6808', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 10:00:02', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6809', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:00:02', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6810', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:00:02', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6817', '133', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 10:02:48', '0', '新增仓库', null, '63');
INSERT INTO `jsh_log` VALUES ('6818', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 10:06:52', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6819', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:06:52', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6820', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:06:52', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6824', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 10:08:53', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6825', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:08:54', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6826', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:08:54', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6833', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:11', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6834', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:11', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6835', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:11', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6836', '133', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:44', '0', '新增单据', null, '63');
INSERT INTO `jsh_log` VALUES ('6837', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:44', '0', '新增单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6838', '133', '单据明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:10:44', '0', '删除,id:单据明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6839', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:11:29', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6841', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:12:36', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6843', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:04', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6844', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:04', '0', '修改,id:,headerId:105财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6845', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:04', '0', '删除,id:财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6846', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:33', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6847', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:33', '0', '修改,id:,headerId:106财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6848', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:33', '0', '删除,id:财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6849', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:13:59', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6850', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:00', '0', '修改,id:,headerId:107财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6851', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:00', '0', '删除,id:财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6852', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:23', '0', '修改,id:60商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6853', '133', '财务', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:23', '0', '新增财务', null, '63');
INSERT INTO `jsh_log` VALUES ('6854', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:24', '0', '修改,id:,headerId:108财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6855', '133', '财务明细', '0:0:0:0:0:0:0:1', '2020-07-08 10:14:24', '0', '删除,id:财务明细', null, '63');
INSERT INTO `jsh_log` VALUES ('6856', '133', '商品类型', '0:0:0:0:0:0:0:1', '2020-07-08 10:16:57', '0', '新增商品类型', null, '63');
INSERT INTO `jsh_log` VALUES ('6857', '133', '计量单位', '0:0:0:0:0:0:0:1', '2020-07-08 10:18:29', '0', '新增计量单位', null, '63');
INSERT INTO `jsh_log` VALUES ('6858', '133', '计量单位', '0:0:0:0:0:0:0:1', '2020-07-08 10:19:00', '0', '修改,id:17计量单位', null, '63');
INSERT INTO `jsh_log` VALUES ('6859', '133', '计量单位', '0:0:0:0:0:0:0:1', '2020-07-08 10:19:04', '0', '修改,id:17计量单位', null, '63');
INSERT INTO `jsh_log` VALUES ('6860', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 10:20:09', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6861', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 10:20:43', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6862', '133', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 10:21:16', '0', '新增商家', null, '63');
INSERT INTO `jsh_log` VALUES ('6863', '133', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 10:21:58', '0', '新增仓库', null, '63');
INSERT INTO `jsh_log` VALUES ('6864', '133', '收支项目', '0:0:0:0:0:0:0:1', '2020-07-08 10:22:27', '0', '新增收支项目', null, '63');
INSERT INTO `jsh_log` VALUES ('6865', '133', '账户', '0:0:0:0:0:0:0:1', '2020-07-08 10:22:58', '0', '新增账户', null, '63');
INSERT INTO `jsh_log` VALUES ('6866', '133', '经手人', '0:0:0:0:0:0:0:1', '2020-07-08 10:23:35', '0', '新增经手人', null, '63');
INSERT INTO `jsh_log` VALUES ('6867', '133', '经手人', '0:0:0:0:0:0:0:1', '2020-07-08 10:23:48', '0', '新增经手人', null, '63');
INSERT INTO `jsh_log` VALUES ('6868', '133', '经手人', '0:0:0:0:0:0:0:1', '2020-07-08 10:23:57', '0', '新增经手人', null, '63');
INSERT INTO `jsh_log` VALUES ('6869', '133', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 10:24:29', '0', '新增角色', null, '63');
INSERT INTO `jsh_log` VALUES ('6870', '133', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 10:24:52', '0', '新增关联关系', null, '63');
INSERT INTO `jsh_log` VALUES ('6871', '133', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 10:25:40', '0', '修改,id:71关联关系', null, '63');
INSERT INTO `jsh_log` VALUES ('6872', '133', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:26:28', '0', '修改,id:133用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6873', '63', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:29:10', '0', '登录,id:63用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6874', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:44:21', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6875', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:48:50', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6876', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:49:23', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6877', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:51:14', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6878', '120', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 10:52:49', '0', '修改,id:70关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6879', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 10:53:22', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6880', '135', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:01:39', '0', '登录,id:135用户', null, '135');
INSERT INTO `jsh_log` VALUES ('6881', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:02:57', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6882', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 11:09:25', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6883', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:16:24', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6884', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:16:24', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6885', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 11:18:09', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6886', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 11:18:26', '0', '删除,id:57,55商家', null, null);
INSERT INTO `jsh_log` VALUES ('6887', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 11:20:42', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6888', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 11:20:42', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6889', '134', '账户', '0:0:0:0:0:0:0:1', '2020-07-08 11:21:46', '0', '新增账户', null, null);
INSERT INTO `jsh_log` VALUES ('6890', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 11:22:25', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6891', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 11:23:39', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('6892', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 11:23:53', '0', '修改,id:13机构', null, null);
INSERT INTO `jsh_log` VALUES ('6893', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:25:05', '0', '修改,id:0用户', null, null);
INSERT INTO `jsh_log` VALUES ('6894', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:36:32', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6895', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:36:32', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6896', '136', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:39:14', '0', '登录,id:136用户', null, '136');
INSERT INTO `jsh_log` VALUES ('6897', '136', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:39:47', '0', '新增用户', null, '136');
INSERT INTO `jsh_log` VALUES ('6898', '136', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:39:57', '0', '删除,id:137用户', null, '136');
INSERT INTO `jsh_log` VALUES ('6899', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:41:28', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6900', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:42:16', '0', '新增用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6901', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:42:20', '0', '删除,id:138用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6902', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 11:52:42', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6903', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 11:54:09', '0', '新增商家', null, '134');
INSERT INTO `jsh_log` VALUES ('6904', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 11:55:07', '0', '新增商家', null, '134');
INSERT INTO `jsh_log` VALUES ('6905', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 11:55:17', '0', '删除,id:84商家', null, '134');
INSERT INTO `jsh_log` VALUES ('6906', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:02:52', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6907', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:02:52', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6908', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:08:10', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6909', '134', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:08:53', '0', '修改,id:70关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6910', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:09:20', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6911', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:10:22', '0', '登录,id:134用户', null, '134');
INSERT INTO `jsh_log` VALUES ('6912', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:10:52', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6913', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:11:55', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6914', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:12:44', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6915', '134', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:13:17', '0', '修改,id:70关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6916', '132', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:13:47', '0', '登录,id:132用户', null, '63');
INSERT INTO `jsh_log` VALUES ('6917', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:14:07', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6918', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:14:28', '0', '删除,id:132用户', null, null);
INSERT INTO `jsh_log` VALUES ('6919', '120', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:15:26', '0', '登录,id:120用户', null, null);
INSERT INTO `jsh_log` VALUES ('6920', '120', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:16:52', '0', '修改,id:70关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6921', '120', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:17:16', '0', '修改,id:70关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6922', '139', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:17:57', '0', '登录,id:139用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6923', '139', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:18:12', '0', '新增用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6924', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:18:26', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6925', '140', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:19:46', '0', '登录,id:140用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6926', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:20:04', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6927', '134', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:20:17', '0', '新增关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6928', '140', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:20:28', '0', '登录,id:140用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6929', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:20:48', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6930', '134', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:21:00', '0', '修改,id:75关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6931', '139', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:21:55', '0', '登录,id:139用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6932', '139', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:22:13', '0', '新增用户', null, '139');
INSERT INTO `jsh_log` VALUES ('6933', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:22:25', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6934', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:25:43', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6935', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:27:19', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6936', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:27:39', '0', '修改,id:85商家', null, null);
INSERT INTO `jsh_log` VALUES ('6937', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:28:50', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6938', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:29:01', '0', '删除,id:86商家', null, null);
INSERT INTO `jsh_log` VALUES ('6939', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 12:29:57', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6940', '134', '账户', '0:0:0:0:0:0:0:1', '2020-07-08 12:30:43', '0', '新增账户', null, null);
INSERT INTO `jsh_log` VALUES ('6941', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 12:31:19', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6942', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 12:32:12', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('6943', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:35:33', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6944', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:37:13', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6945', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:37:29', '0', '删除,id:87商家', null, null);
INSERT INTO `jsh_log` VALUES ('6946', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:37:49', '0', '修改,id:80商家', null, null);
INSERT INTO `jsh_log` VALUES ('6947', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:37:56', '0', '修改,id:80商家', null, null);
INSERT INTO `jsh_log` VALUES ('6948', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 12:39:29', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6949', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 12:39:42', '0', '删除,id:24仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6950', '134', '序列号', '0:0:0:0:0:0:0:1', '2020-07-08 12:40:22', '0', '删除,id:21序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6951', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 12:40:32', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6952', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 12:41:21', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('6953', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:20', '0', '删除,id:239单据', null, null);
INSERT INTO `jsh_log` VALUES ('6954', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:20', '0', '删除,id:239单据', null, null);
INSERT INTO `jsh_log` VALUES ('6955', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:20', '0', '删除,id:239单据', null, null);
INSERT INTO `jsh_log` VALUES ('6956', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:31', '0', '删除,id:272,262单据', null, null);
INSERT INTO `jsh_log` VALUES ('6957', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:31', '0', '删除,id:272单据', null, null);
INSERT INTO `jsh_log` VALUES ('6958', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:31', '0', '删除,id:272单据', null, null);
INSERT INTO `jsh_log` VALUES ('6959', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:31', '0', '删除,id:262单据', null, null);
INSERT INTO `jsh_log` VALUES ('6960', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 12:42:31', '0', '删除,id:262单据', null, null);
INSERT INTO `jsh_log` VALUES ('6961', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:48:26', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6962', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:49:52', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6963', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:50:07', '0', '修改,id:88商家', null, null);
INSERT INTO `jsh_log` VALUES ('6964', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:50:19', '0', '修改,id:88,85商家', null, null);
INSERT INTO `jsh_log` VALUES ('6965', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:51:01', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6966', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 12:52:13', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6967', '134', '账户', '0:0:0:0:0:0:0:1', '2020-07-08 12:53:46', '0', '删除,id:4账户', null, null);
INSERT INTO `jsh_log` VALUES ('6968', '134', '序列号', '0:0:0:0:0:0:0:1', '2020-07-08 12:54:09', '0', '删除,id:22序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6969', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 12:54:19', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6970', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 12:55:32', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('6971', '134', '关联关系', '0:0:0:0:0:0:0:1', '2020-07-08 12:56:17', '0', '新增关联关系', null, null);
INSERT INTO `jsh_log` VALUES ('6972', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:58:07', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6973', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 12:58:56', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6974', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:00:29', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6975', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:00:46', '0', '修改,id:91商家', null, null);
INSERT INTO `jsh_log` VALUES ('6976', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:00:54', '0', '修改,id:88,85商家', null, null);
INSERT INTO `jsh_log` VALUES ('6977', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:00:59', '0', '删除,id:91商家', null, null);
INSERT INTO `jsh_log` VALUES ('6978', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:01:43', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6979', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:02:37', '0', '修改,id:90商家', null, null);
INSERT INTO `jsh_log` VALUES ('6980', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:02:43', '0', '修改,id:90商家', null, null);
INSERT INTO `jsh_log` VALUES ('6981', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 13:03:56', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6982', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 13:04:29', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6983', '134', '序列号', '0:0:0:0:0:0:0:1', '2020-07-08 13:04:35', '0', '删除,id:23序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6984', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 13:05:34', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('6985', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 13:05:42', '0', '删除,id:16,17机构', null, null);
INSERT INTO `jsh_log` VALUES ('6986', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:08:00', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6987', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:08:00', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6988', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:12:39', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('6989', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:14:14', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6990', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:14:25', '0', '修改,id:93商家', null, null);
INSERT INTO `jsh_log` VALUES ('6991', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:14:32', '0', '删除,id:93商家', null, null);
INSERT INTO `jsh_log` VALUES ('6992', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:15:09', '0', '新增商家', null, null);
INSERT INTO `jsh_log` VALUES ('6993', '134', '商家', '0:0:0:0:0:0:0:1', '2020-07-08 13:15:24', '0', '删除,id:94,92,89商家', null, null);
INSERT INTO `jsh_log` VALUES ('6994', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 13:16:46', '0', '新增仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6995', '134', '仓库', '0:0:0:0:0:0:0:1', '2020-07-08 13:17:03', '0', '删除,id:22仓库', null, null);
INSERT INTO `jsh_log` VALUES ('6996', '134', '序列号', '0:0:0:0:0:0:0:1', '2020-07-08 13:17:48', '0', '删除,id:20序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6997', '134', '序列号', '0:0:0:0:0:0:0:1', '2020-07-08 13:17:51', '0', '删除,id:24序列号', null, null);
INSERT INTO `jsh_log` VALUES ('6998', '134', '角色', '0:0:0:0:0:0:0:1', '2020-07-08 13:18:05', '0', '新增角色', null, null);
INSERT INTO `jsh_log` VALUES ('6999', '134', '机构', '0:0:0:0:0:0:0:1', '2020-07-08 13:18:45', '0', '新增机构', null, null);
INSERT INTO `jsh_log` VALUES ('7000', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 13:19:56', '0', '修改,id:257,256,234单据', null, null);
INSERT INTO `jsh_log` VALUES ('7001', '134', '单据', '0:0:0:0:0:0:0:1', '2020-07-08 13:20:34', '0', '修改,id:265,201单据', null, null);
INSERT INTO `jsh_log` VALUES ('7002', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:33:19', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7003', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:33:19', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7004', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 13:37:35', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7005', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 17:48:43', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7006', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:09:56', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7007', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:22:06', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7008', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:35:24', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7009', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:42:51', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7010', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:47:36', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7011', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:52:28', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7012', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:56:32', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7013', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 20:59:03', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7014', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 21:28:06', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7015', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 21:52:42', '0', '登录,id:134用户', null, null);
INSERT INTO `jsh_log` VALUES ('7016', '134', '用户', '0:0:0:0:0:0:0:1', '2020-07-08 21:52:42', '0', '登录,id:134用户', null, null);

-- ----------------------------
-- Table structure for `jsh_material`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_material`;
CREATE TABLE `jsh_material` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CategoryId` bigint(20) DEFAULT NULL COMMENT '产品类型',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `Mfrs` varchar(50) DEFAULT NULL COMMENT '制造商',
  `Packing` decimal(24,6) DEFAULT NULL COMMENT '包装（KG/包）',
  `SafetyStock` decimal(24,6) DEFAULT NULL COMMENT '安全存量（KG）',
  `Model` varchar(50) DEFAULT NULL COMMENT '型号',
  `Standard` varchar(50) DEFAULT NULL COMMENT '规格',
  `Color` varchar(50) DEFAULT NULL COMMENT '颜色',
  `Unit` varchar(50) DEFAULT NULL COMMENT '单位-单个',
  `Remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `RetailPrice` decimal(24,6) DEFAULT NULL COMMENT '零售价',
  `LowPrice` decimal(24,6) DEFAULT NULL COMMENT '最低售价',
  `PresetPriceOne` decimal(24,6) DEFAULT NULL COMMENT '预设售价一',
  `PresetPriceTwo` decimal(24,6) DEFAULT NULL COMMENT '预设售价二',
  `UnitId` bigint(20) DEFAULT NULL COMMENT '计量单位Id',
  `FirstOutUnit` varchar(50) DEFAULT NULL COMMENT '首选出库单位',
  `FirstInUnit` varchar(50) DEFAULT NULL COMMENT '首选入库单位',
  `PriceStrategy` varchar(500) DEFAULT NULL COMMENT '价格策略',
  `Enabled` bit(1) DEFAULT NULL COMMENT '启用 0-禁用  1-启用',
  `OtherField1` varchar(50) DEFAULT NULL COMMENT '自定义1',
  `OtherField2` varchar(50) DEFAULT NULL COMMENT '自定义2',
  `OtherField3` varchar(50) DEFAULT NULL COMMENT '自定义3',
  `enableSerialNumber` varchar(1) DEFAULT '0' COMMENT '是否开启序列号，0否，1是',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`),
  KEY `FK675951272AB6672C` (`CategoryId`),
  KEY `UnitId` (`UnitId`)
) ENGINE=InnoDB AUTO_INCREMENT=596 DEFAULT CHARSET=utf8 COMMENT='产品表';

-- ----------------------------
-- Records of jsh_material
-- ----------------------------
INSERT INTO `jsh_material` VALUES ('564', '14', '商品1', '', null, null, 'sp1', '', '', '个', '', '22.000000', '22.000000', '22.000000', '22.000000', null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"enableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '1', '0');
INSERT INTO `jsh_material` VALUES ('565', '14', '商品2', '', null, null, 'sp2', '', '', '个', '', '44.000000', '44.000000', '44.000000', '44.000000', null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"1\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '1', '1', '0');
INSERT INTO `jsh_material` VALUES ('566', '15', '商品666', '', null, null, 'sp666', '', '', '个', '', '5.000000', '4.000000', '3.000000', '2.000000', null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '117', '0');
INSERT INTO `jsh_material` VALUES ('567', null, '商品1', '', null, null, 'dsp1', '', '', '个', '', '11.000000', '11.000000', '11.000000', '11.000000', null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '115', '0');
INSERT INTO `jsh_material` VALUES ('568', '17', '商品1', '', null, '100.000000', 'sp1', '', '', '个', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('569', '17', '商品2', '', null, '200.000000', 'sp2', '', '', '只', '', '5.000000', '5.000000', '5.000000', '5.000000', null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('570', '17', '商品3', '', null, '300.000000', 'sp3', '', '', '个', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('571', null, '商品4', '', null, null, 'sp4', '', '', '', '', null, null, null, null, '15', '个', '箱', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"3\",\"LowPrice\":\"2\",\"PresetPriceOne\":\"2\",\"PresetPriceTwo\":\"2\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"36\",\"LowPrice\":\"24\",\"PresetPriceOne\":\"24\",\"PresetPriceTwo\":\"24\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('572', null, '234234', '', null, null, '234234', '', '', '', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('573', null, '12312', '', null, null, '12', '', '', '', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('574', null, '商品5', '', null, null, '213qw', '', '', '个', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('575', null, '商品6', '', null, null, 'sp6', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('576', null, '商品7', '', null, null, 'sp7', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('577', null, '商品8', '', null, null, 'sp8', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('578', '17', '商品9', '', null, null, 'sp9', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('579', null, '商品17', '', null, null, 'sp17', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('580', null, '15', '', null, null, '15', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('581', null, '16', '', null, null, '16', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('582', null, '商品20', '', null, null, 'sp2', '', '', '个', '', null, null, null, null, null, '', '', '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('583', null, 'wer', '', null, null, 'rqwe', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('584', null, 'sfds', '', null, null, 'a2233', '12', '2', 'ge', '', '1.000000', '2.000000', '3.000000', '4.000000', null, '', '', '[{\"basic\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('585', null, 'asdf', '', null, null, 'adsfasdf', '', '', '', '', null, null, null, null, '15', '', '', '[{\"basic\":{\"Unit\":\"个\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"箱\",\"RetailPrice\":\"\",\"LowPrice\":\"\",\"PresetPriceOne\":\"\",\"PresetPriceTwo\":\"\"}}]', '', '', '', '', '0', '63', '1');
INSERT INTO `jsh_material` VALUES ('586', null, '序列号商品测试', '', null, null, 'xlh123', '', '', '个', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"1\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '1', '63', '0');
INSERT INTO `jsh_material` VALUES ('587', null, '商品test1', '', null, null, '', 'test1', '', '个', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '1', '63', '0');
INSERT INTO `jsh_material` VALUES ('588', null, '新增商品1', '新增商品1', null, '1.000000', '1', '1', '1', 'kg', '新增商品1', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('589', null, 'zzzz', '', null, null, '', 'zzzz', '', 'kg', '1010', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('590', null, '1', '1', null, null, '1', '1', '1', '1', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('591', null, '2', '', null, null, '2', '2', '2', '2', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('592', null, '3', '', null, null, '3', '3', '3', '3', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('593', null, '4', '', null, null, '4', '4', '4', '4', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('594', '17', '铁锹', '', null, null, 'KG9001', '中', '绿', '把', '', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');
INSERT INTO `jsh_material` VALUES ('595', null, '商品3', '', null, null, '', '', '', '个', '333333333333', null, null, null, null, null, null, null, '[{\"basic\":{\"Unit\":\"\",\"EnableSerialNumber\":\"0\"}},{\"other\":{\"Unit\":\"\"}}]', '', '', '', '', '0', '63', '0');

-- ----------------------------
-- Table structure for `jsh_material_extend`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_material_extend`;
CREATE TABLE `jsh_material_extend` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_id` bigint(20) DEFAULT NULL COMMENT '商品id',
  `bar_code` varchar(50) DEFAULT NULL COMMENT '商品条码',
  `commodity_unit` varchar(50) DEFAULT NULL COMMENT '商品单位',
  `purchase_decimal` decimal(24,6) DEFAULT NULL COMMENT '采购价格',
  `commodity_decimal` decimal(24,6) DEFAULT NULL COMMENT '零售价格',
  `wholesale_decimal` decimal(24,6) DEFAULT NULL COMMENT '销售价格',
  `low_decimal` decimal(24,6) DEFAULT NULL COMMENT '最低售价',
  `create_time` datetime DEFAULT NULL COMMENT '创建日期',
  `create_serial` varchar(50) DEFAULT NULL COMMENT '创建人编码',
  `update_serial` varchar(50) DEFAULT NULL COMMENT '更新人编码',
  `update_time` bigint(20) DEFAULT NULL COMMENT '更新时间戳',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='产品价格扩展';

-- ----------------------------
-- Records of jsh_material_extend
-- ----------------------------
INSERT INTO `jsh_material_extend` VALUES ('1', '587', '1000', '个', '11.000000', '22.000000', '22.000000', '22.000000', '2020-02-20 23:22:03', 'jsh', 'jsh', '1582212123066', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('2', '568', '1001', '个', '11.000000', '15.000000', '15.000000', '15.000000', '2020-02-20 23:44:57', 'jsh', 'jsh', '1582213497098', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('3', '569', '1002', '只', '10.000000', '15.000000', '15.000000', '13.000000', '2020-02-20 23:45:15', 'jsh', 'jsh', '1582213514731', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('4', '570', '1003', '个', '8.000000', '15.000000', '14.000000', '13.000000', '2020-02-20 23:45:37', 'jsh', 'jsh', '1582213536875', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('5', '577', '1004', '个', '10.000000', '20.000000', '20.000000', '20.000000', '2020-02-20 23:46:36', 'jsh', 'jsh', '1582213596494', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('6', '577', '1005', '箱', '120.000000', '240.000000', '240.000000', '240.000000', '2020-02-20 23:46:36', 'jsh', 'jsh', '1582213596497', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('7', '579', '1006', '个', '20.000000', '30.000000', '30.000000', '30.000000', '2020-02-20 23:47:04', 'jsh', 'jsh', '1582213624089', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('8', '579', '1007', '箱', '240.000000', '360.000000', '360.000000', '360.000000', '2020-02-20 23:47:04', 'jsh', 'jsh', '1582213624092', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('9', '586', '1008', '个', '12.000000', '15.000000', '15.000000', '15.000000', '2020-02-20 23:47:23', 'jsh', 'jsh', '1582213643084', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('10', '588', '1009', '1009', '1009.000000', '1009.000000', '1009.000000', '1009.000000', '2020-07-07 13:57:34', 'ljy', 'ljy', '1594148253588', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('11', '589', '1010', 'kg', '1010.000000', '1010.000000', '1010.000000', '1010.000000', '2020-07-07 14:05:26', 'ljy', 'ljy', '1594148726351', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('12', '590', '1011', '1', '1.000000', '1.000000', '1.000000', '1.000000', '2020-07-07 20:06:03', 'ljy', 'ljy', '1594152363010', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('13', '591', '1012', '2', '2.000000', '2.000000', '2.000000', '2.000000', '2020-07-07 20:12:48', 'ljy', 'ljy', '1594152767768', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('14', '592', '1013', '3', '3.000000', '3.000000', '3.000000', '3.000000', '2020-07-07 20:17:42', 'ljy', 'ljy', '1594153062148', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('15', '593', '1014', '4', '4.000000', '4.000000', '4.000000', '4.000000', '2020-07-07 20:18:48', 'ljy', 'ljy', '1594153128437', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('16', '594', '1015', '把', '1.000000', '1.000000', '1.000000', '1.000000', '2020-07-08 09:22:52', 'ljy', 'ljy', '1594171372223', '63', '0');
INSERT INTO `jsh_material_extend` VALUES ('17', '595', '1016', '个', '3.000000', '3.000000', '3.000000', '3.000000', '2020-07-08 09:34:09', 'ljy', 'ljy', '1594172049477', '63', '0');

-- ----------------------------
-- Table structure for `jsh_material_stock`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_material_stock`;
CREATE TABLE `jsh_material_stock` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_id` bigint(20) DEFAULT NULL COMMENT '产品id',
  `depot_id` bigint(20) DEFAULT NULL COMMENT '仓库id',
  `number` decimal(24,6) DEFAULT NULL COMMENT '初始库存数量',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_fag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='产品初始库存';

-- ----------------------------
-- Records of jsh_material_stock
-- ----------------------------
INSERT INTO `jsh_material_stock` VALUES ('119', '590', '16', '10.000000', '63', '0');

-- ----------------------------
-- Table structure for `jsh_materialcategory`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialcategory`;
CREATE TABLE `jsh_materialcategory` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `CategoryLevel` smallint(6) DEFAULT NULL COMMENT '等级',
  `ParentId` bigint(20) DEFAULT NULL COMMENT '上级ID',
  `sort` varchar(10) DEFAULT NULL COMMENT '显示顺序',
  `status` varchar(1) DEFAULT '0' COMMENT '状态，0系统默认，1启用，2删除',
  `serial_no` varchar(100) DEFAULT NULL COMMENT '编号',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater` bigint(20) DEFAULT NULL COMMENT '更新人',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`Id`),
  KEY `FK3EE7F725237A77D8` (`ParentId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='产品类型表';

-- ----------------------------
-- Records of jsh_materialcategory
-- ----------------------------
INSERT INTO `jsh_materialcategory` VALUES ('1', '根目录', '1', '-1', null, '2', '1', null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('2', '花边一级A', '1', '1', '', '2', '', '', null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('3', '花边一级B', '1', '1', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('4', '其他', '2', '3', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('5', '其他', '3', '4', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('6', '花边二级A', '2', '2', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('7', '花边三级A', '3', '6', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('8', '花边二级B', '2', '2', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('9', '花边一级C', '1', '1', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('10', '花边三级B', '3', '6', null, '2', null, null, null, null, '2019-03-15 23:09:05', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('11', 'ddddd', null, '-1', '', '1', '', '', '2019-03-15 23:09:13', '63', '2019-03-15 23:09:13', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('12', 'ffffff', null, '11', '', '1', '', '', '2019-03-15 23:09:27', '63', '2019-03-15 23:09:27', '63', null);
INSERT INTO `jsh_materialcategory` VALUES ('13', '目录1', null, '-1', '', '1', '111', '', '2019-03-18 22:45:39', '63', '2019-03-18 22:45:39', '63', '1');
INSERT INTO `jsh_materialcategory` VALUES ('14', '目录2', null, '13', '', '1', '234', '', '2019-03-18 23:39:39', '63', '2019-03-18 23:39:39', '63', '1');
INSERT INTO `jsh_materialcategory` VALUES ('15', '目录1', null, '-1', '', '1', '', '', '2019-03-31 21:53:53', '117', '2019-03-31 21:53:53', '117', '117');
INSERT INTO `jsh_materialcategory` VALUES ('16', 'aaaa', null, '-1', '', '1', '', '', '2019-04-02 22:28:07', '115', '2019-04-02 22:28:07', '115', '115');
INSERT INTO `jsh_materialcategory` VALUES ('17', '目录1', null, '-1', '', '1', '', '', '2019-04-10 22:18:12', '63', '2019-04-10 22:18:12', '63', '63');
INSERT INTO `jsh_materialcategory` VALUES ('18', '目录2', null, '-1', '002', '2', '002', '002', '2020-07-07 14:08:21', '133', '2020-07-07 14:08:34', '133', '63');
INSERT INTO `jsh_materialcategory` VALUES ('19', '农具', null, '-1', '001', '1', '001', '001', '2020-07-08 10:16:57', '133', '2020-07-08 10:16:57', '133', '63');

-- ----------------------------
-- Table structure for `jsh_materialproperty`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_materialproperty`;
CREATE TABLE `jsh_materialproperty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nativeName` varchar(50) DEFAULT NULL COMMENT '原始名称',
  `enabled` bit(1) DEFAULT NULL COMMENT '是否启用',
  `sort` varchar(10) DEFAULT NULL COMMENT '排序',
  `anotherName` varchar(50) DEFAULT NULL COMMENT '别名',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='产品扩展字段表';

-- ----------------------------
-- Records of jsh_materialproperty
-- ----------------------------
INSERT INTO `jsh_materialproperty` VALUES ('1', '规格', '', '02', '规格', '0');
INSERT INTO `jsh_materialproperty` VALUES ('2', '颜色', '', '01', '颜色', '0');
INSERT INTO `jsh_materialproperty` VALUES ('3', '制造商', '', '03', '制造商', '0');
INSERT INTO `jsh_materialproperty` VALUES ('4', '自定义1', '\0', '04', '自定义1', '0');
INSERT INTO `jsh_materialproperty` VALUES ('5', '自定义2', '\0', '05', '自定义2', '0');
INSERT INTO `jsh_materialproperty` VALUES ('6', '自定义3', '\0', '06', '自定义3', '0');

-- ----------------------------
-- Table structure for `jsh_msg`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_msg`;
CREATE TABLE `jsh_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `msg_title` varchar(100) DEFAULT NULL COMMENT '消息标题',
  `msg_content` varchar(500) DEFAULT NULL COMMENT '消息内容',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `type` varchar(20) DEFAULT NULL COMMENT '消息类型',
  `status` varchar(1) DEFAULT NULL COMMENT '状态，1未读 2已读',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='消息表';

-- ----------------------------
-- Records of jsh_msg
-- ----------------------------
INSERT INTO `jsh_msg` VALUES ('2', '标题1', '内容1', '2019-09-10 00:11:39', '类型1', '2', '63', '0');

-- ----------------------------
-- Table structure for `jsh_orga_user_rel`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_orga_user_rel`;
CREATE TABLE `jsh_orga_user_rel` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `orga_id` bigint(20) NOT NULL COMMENT '机构id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `user_blng_orga_dspl_seq` varchar(20) DEFAULT NULL COMMENT '用户在所属机构中显示顺序',
  `delete_flag` char(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater` bigint(20) DEFAULT NULL COMMENT '更新人',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='机构用户关系表';

-- ----------------------------
-- Records of jsh_orga_user_rel
-- ----------------------------
INSERT INTO `jsh_orga_user_rel` VALUES ('1', '9', '64', '', '0', null, null, '2019-03-15 23:03:39', '63', null);
INSERT INTO `jsh_orga_user_rel` VALUES ('2', '3', '65', null, '0', null, null, null, null, null);
INSERT INTO `jsh_orga_user_rel` VALUES ('3', '3', '67', null, '0', null, null, null, null, null);
INSERT INTO `jsh_orga_user_rel` VALUES ('4', '4', '84', null, '0', null, null, null, null, null);
INSERT INTO `jsh_orga_user_rel` VALUES ('5', '5', '86', null, '0', null, null, null, null, null);
INSERT INTO `jsh_orga_user_rel` VALUES ('6', '3', '91', '', '0', '2019-03-12 21:55:28', '63', '2019-03-12 21:55:28', '63', null);
INSERT INTO `jsh_orga_user_rel` VALUES ('7', '9', '95', '', '0', '2019-03-15 23:03:22', '63', '2019-03-15 23:03:22', '63', null);
INSERT INTO `jsh_orga_user_rel` VALUES ('8', '9', '96', '', '0', '2019-03-17 23:32:08', '63', '2019-03-17 23:32:08', '63', null);
INSERT INTO `jsh_orga_user_rel` VALUES ('9', '10', '117', '', '0', '2019-03-31 21:53:03', '117', '2019-03-31 21:53:12', '117', '117');
INSERT INTO `jsh_orga_user_rel` VALUES ('10', '12', '131', '', '0', '2019-12-28 12:13:15', '63', '2019-12-28 12:13:15', '63', '63');
INSERT INTO `jsh_orga_user_rel` VALUES ('11', '13', '133', '', '0', '2020-07-07 07:27:06', '63', '2020-07-08 10:26:28', '133', '63');

-- ----------------------------
-- Table structure for `jsh_organization`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_organization`;
CREATE TABLE `jsh_organization` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `org_no` varchar(20) DEFAULT NULL COMMENT '机构编号',
  `org_full_name` varchar(500) DEFAULT NULL COMMENT '机构全称',
  `org_abr` varchar(20) DEFAULT NULL COMMENT '机构简称',
  `org_tpcd` varchar(9) DEFAULT NULL COMMENT '机构类型',
  `org_stcd` char(1) DEFAULT NULL COMMENT '机构状态,1未营业、2正常营业、3暂停营业、4终止营业、5已除名',
  `org_parent_no` varchar(20) DEFAULT NULL COMMENT '机构父节点编号',
  `sort` varchar(20) DEFAULT NULL COMMENT '机构显示顺序',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater` bigint(20) DEFAULT NULL COMMENT '更新人',
  `org_create_time` datetime DEFAULT NULL COMMENT '机构创建时间',
  `org_stop_time` datetime DEFAULT NULL COMMENT '机构停运时间',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='机构表';

-- ----------------------------
-- Records of jsh_organization
-- ----------------------------
INSERT INTO `jsh_organization` VALUES ('1', '01', '根机构', '根机构', null, '5', '-1', '1', '根机构，初始化存在', null, null, '2019-03-15 23:01:21', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('2', null, '销售', '销售', null, '5', '01', '1', '机构表初始化', null, null, '2019-03-15 23:01:21', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('3', null, 'sdf444', 'sdf444', null, '5', '01', '2', '机构表初始化', null, null, '2019-03-15 23:01:19', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('4', null, '1231', '1231', null, '5', '01', '3', '机构表初始化', null, null, '2019-03-15 23:01:19', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('5', null, '23', '23', null, '5', '01', '4', '机构表初始化', null, null, '2019-03-15 23:01:19', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('6', '4444', 'abcd', 'abcd', null, '1', '-1', '', '', '2019-03-15 23:01:30', '63', '2019-03-15 23:01:47', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('7', '123', 'bbbb', 'bbbb', null, '1', 'abcd', '', '', '2019-03-15 23:01:42', '63', '2019-03-15 23:01:42', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('8', 'ddddd', 'ddddd', 'ddddd', null, '1', '4444', '', '', '2019-03-15 23:02:02', '63', '2019-03-15 23:02:02', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('9', '555', 'dddddddddd', 'dddddddddd', null, '1', 'ddddd', '', '', '2019-03-15 23:02:16', '63', '2019-03-15 23:02:16', '63', null, null, null);
INSERT INTO `jsh_organization` VALUES ('10', '23124', 'gaga', 'gaga', null, '1', '-1', '11', '', '2019-03-31 21:52:31', '117', '2019-03-31 21:52:31', '117', null, null, '117');
INSERT INTO `jsh_organization` VALUES ('11', '12312', 'fsadfasdf', 'fsadfasdf', null, '1', '23124', '12312', '', '2019-03-31 21:52:52', '117', '2019-03-31 21:52:52', '117', null, null, '117');
INSERT INTO `jsh_organization` VALUES ('12', '001', '测试机构', '测试机构', null, '2', '-1', '001', '', '2019-12-28 12:13:01', '63', '2019-12-28 12:13:01', '63', null, null, '63');
INSERT INTO `jsh_organization` VALUES ('13', '01', '连队1', '连队1', null, '3', '-1', '10', '', '2020-07-08 09:19:00', '133', '2020-07-08 11:23:53', '134', '2020-07-08 09:18:40', null, '63');
INSERT INTO `jsh_organization` VALUES ('14', '170001', '连队2', '队2', null, '2', '-1', '005', '无', '2020-07-08 11:23:39', '134', '2020-07-08 11:23:39', '134', '2020-07-01 11:23:07', null, null);
INSERT INTO `jsh_organization` VALUES ('15', '1003', '连队3', '队3', null, '2', '-1', '0003', '', '2020-07-08 12:32:12', '134', '2020-07-08 12:32:12', '134', '2020-07-01 12:32:05', null, null);
INSERT INTO `jsh_organization` VALUES ('16', '100004', '连队4', '队4', null, '5', '-1', '00014', '', '2020-07-08 12:41:21', '134', '2020-07-08 13:05:42', '134', '2020-07-01 12:41:15', null, null);
INSERT INTO `jsh_organization` VALUES ('17', '1000045', '队列5', '队5', null, '5', '-1', '00015', '', '2020-07-08 12:55:32', '134', '2020-07-08 13:05:42', '134', '2020-07-01 12:55:28', null, null);
INSERT INTO `jsh_organization` VALUES ('18', '100006', '连队6', '队6', null, '2', '-1', '00006', '', '2020-07-08 13:05:34', '134', '2020-07-08 13:05:34', '134', '2020-07-01 13:05:29', null, null);
INSERT INTO `jsh_organization` VALUES ('19', '100009', '连队9', '队9', null, '2', '-1', '00009', '', '2020-07-08 13:18:45', '134', '2020-07-08 13:18:45', '134', '2020-06-01 13:18:38', null, null);

-- ----------------------------
-- Table structure for `jsh_person`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_person`;
CREATE TABLE `jsh_person` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Type` varchar(20) DEFAULT NULL COMMENT '类型',
  `Name` varchar(50) DEFAULT NULL COMMENT '姓名',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='经手人表';

-- ----------------------------
-- Records of jsh_person
-- ----------------------------
INSERT INTO `jsh_person` VALUES ('3', '财务员', '王五-财务', null, '0');
INSERT INTO `jsh_person` VALUES ('4', '财务员', '赵六-财务', null, '0');
INSERT INTO `jsh_person` VALUES ('5', '业务员', '小李', null, '0');
INSERT INTO `jsh_person` VALUES ('6', '业务员', '小军', null, '0');
INSERT INTO `jsh_person` VALUES ('7', '业务员', '小曹', null, '0');
INSERT INTO `jsh_person` VALUES ('8', '仓管员', '小季', '1', '0');
INSERT INTO `jsh_person` VALUES ('9', '财务员', '小月', '1', '0');
INSERT INTO `jsh_person` VALUES ('10', '仓管员', '小张', '117', '0');
INSERT INTO `jsh_person` VALUES ('11', '业务员', '晓丽', '117', '0');
INSERT INTO `jsh_person` VALUES ('12', '财务员', '小草', '117', '0');
INSERT INTO `jsh_person` VALUES ('13', '业务员', '经手人1', '115', '0');
INSERT INTO `jsh_person` VALUES ('14', '业务员', '小李', '63', '0');
INSERT INTO `jsh_person` VALUES ('15', '仓管员', '小军', '63', '0');
INSERT INTO `jsh_person` VALUES ('16', '财务员', '小夏', '63', '0');
INSERT INTO `jsh_person` VALUES ('17', '仓管员', '李佳忆', '63', '0');
INSERT INTO `jsh_person` VALUES ('18', '财务员', '李佳忆', '63', '0');
INSERT INTO `jsh_person` VALUES ('19', '业务员', '李佳忆', '63', '0');

-- ----------------------------
-- Table structure for `jsh_role`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_role`;
CREATE TABLE `jsh_role` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Name` varchar(50) DEFAULT NULL COMMENT '名称',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `value` varchar(200) DEFAULT NULL COMMENT '值',
  `description` varchar(100) DEFAULT NULL COMMENT '描述',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of jsh_role
-- ----------------------------
INSERT INTO `jsh_role` VALUES ('4', '管理员', null, null, null, null, '0');
INSERT INTO `jsh_role` VALUES ('5', '仓管员', null, null, null, null, '0');
INSERT INTO `jsh_role` VALUES ('10', '租户', null, null, null, null, '0');
INSERT INTO `jsh_role` VALUES ('12', '角色123', null, null, null, '117', '1');
INSERT INTO `jsh_role` VALUES ('13', '角色test', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('14', '44444', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('15', 'laoba角色', null, null, null, '115', '1');
INSERT INTO `jsh_role` VALUES ('16', '测试角色123', null, null, null, '63', '1');
INSERT INTO `jsh_role` VALUES ('17', '测试', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('18', '管理员', null, null, null, '63', '0');
INSERT INTO `jsh_role` VALUES ('19', 'test', null, null, null, null, '0');
INSERT INTO `jsh_role` VALUES ('20', '测试员', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('21', '测试员2', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('22', 'test2', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('23', '测试员2', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('24', '测试员3', null, null, null, null, '1');
INSERT INTO `jsh_role` VALUES ('25', '财务管理员', null, null, null, null, '0');

-- ----------------------------
-- Table structure for `jsh_sequence`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_sequence`;
CREATE TABLE `jsh_sequence` (
  `seq_name` varchar(50) NOT NULL COMMENT '序列名称',
  `min_value` bigint(20) NOT NULL COMMENT '最小值',
  `max_value` bigint(20) NOT NULL COMMENT '最大值',
  `current_val` bigint(20) NOT NULL COMMENT '当前值',
  `increment_val` int(11) NOT NULL DEFAULT '1' COMMENT '增长步数',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`seq_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='单据编号表';

-- ----------------------------
-- Records of jsh_sequence
-- ----------------------------
INSERT INTO `jsh_sequence` VALUES ('depot_number_seq', '1', '999999999999999999', '416', '1', '单据编号sequence');

-- ----------------------------
-- Table structure for `jsh_serial_number`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_serial_number`;
CREATE TABLE `jsh_serial_number` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `material_Id` bigint(20) DEFAULT NULL COMMENT '产品表id',
  `serial_Number` varchar(64) DEFAULT NULL COMMENT '序列号',
  `is_Sell` varchar(1) DEFAULT '0' COMMENT '是否卖出，0未卖出，1卖出',
  `remark` varchar(1024) DEFAULT NULL COMMENT '备注',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  `create_Time` datetime DEFAULT NULL COMMENT '创建时间',
  `creator` bigint(20) DEFAULT NULL COMMENT '创建人',
  `update_Time` datetime DEFAULT NULL COMMENT '更新时间',
  `updater` bigint(20) DEFAULT NULL COMMENT '更新人',
  `depothead_Id` bigint(20) DEFAULT NULL COMMENT '单据主表id，用于跟踪序列号流向',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='序列号表';

-- ----------------------------
-- Records of jsh_serial_number
-- ----------------------------
INSERT INTO `jsh_serial_number` VALUES ('105', '586', '12312323423223', '0', '', '0', '2019-12-28 12:14:39', '63', '2019-12-28 12:14:39', '63', null, '63');
INSERT INTO `jsh_serial_number` VALUES ('106', '587', '121111111', '0', '121111111', '0', '2020-07-07 14:12:24', '133', '2020-07-07 14:12:24', '133', null, '63');

-- ----------------------------
-- Table structure for `jsh_supplier`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_supplier`;
CREATE TABLE `jsh_supplier` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `supplier` varchar(255) NOT NULL COMMENT '供应商名称',
  `contacts` varchar(100) DEFAULT NULL COMMENT '联系人',
  `phonenum` varchar(30) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `description` varchar(500) DEFAULT NULL COMMENT '备注',
  `isystem` tinyint(4) DEFAULT NULL COMMENT '是否系统自带 0==系统 1==非系统',
  `type` varchar(20) DEFAULT NULL COMMENT '类型',
  `enabled` bit(1) DEFAULT NULL COMMENT '启用',
  `AdvanceIn` decimal(24,6) DEFAULT '0.000000' COMMENT '预收款',
  `BeginNeedGet` decimal(24,6) DEFAULT NULL COMMENT '期初应收',
  `BeginNeedPay` decimal(24,6) DEFAULT NULL COMMENT '期初应付',
  `AllNeedGet` decimal(24,6) DEFAULT NULL COMMENT '累计应收',
  `AllNeedPay` decimal(24,6) DEFAULT NULL COMMENT '累计应付',
  `fax` varchar(30) DEFAULT NULL COMMENT '传真',
  `telephone` varchar(30) DEFAULT NULL COMMENT '手机',
  `address` varchar(50) DEFAULT NULL COMMENT '地址',
  `taxNum` varchar(50) DEFAULT NULL COMMENT '纳税人识别号',
  `bankName` varchar(50) DEFAULT NULL COMMENT '开户行',
  `accountNumber` varchar(50) DEFAULT NULL COMMENT '账号',
  `taxRate` decimal(24,6) DEFAULT NULL COMMENT '税率',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='供应商/客户信息表';

-- ----------------------------
-- Records of jsh_supplier
-- ----------------------------
INSERT INTO `jsh_supplier` VALUES ('47', '供应商1', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '1', '0');
INSERT INTO `jsh_supplier` VALUES ('48', '客户1', '', '', '', '', null, '客户', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '1', '0');
INSERT INTO `jsh_supplier` VALUES ('49', 'ddddd123', '', '', '', '', null, '会员', '', '6.000000', null, null, null, null, '', '', '', '', '', '', null, '1', '0');
INSERT INTO `jsh_supplier` VALUES ('50', '供应商2', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '1', '0');
INSERT INTO `jsh_supplier` VALUES ('51', '供应商1', '小周', '', '', '', null, '供应商', '', '0.000000', null, '2000.000000', null, null, '', '', '', '', '', '', null, '117', '0');
INSERT INTO `jsh_supplier` VALUES ('52', '客户123', '', '', '', '', null, '客户', '', '0.000000', '1000.000000', null, null, null, '', '', '', '', '', '', null, '117', '0');
INSERT INTO `jsh_supplier` VALUES ('53', '会员123123', '', '', '', '', null, '会员', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '117', '0');
INSERT INTO `jsh_supplier` VALUES ('54', '供应商2222', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '117', '0');
INSERT INTO `jsh_supplier` VALUES ('55', '供应商1', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '115', '1');
INSERT INTO `jsh_supplier` VALUES ('56', '客户666', '', '', '', '', null, '客户', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '115', '0');
INSERT INTO `jsh_supplier` VALUES ('57', '供应商1', '', '', '', '', null, '供应商', '', '0.000000', '0.000000', '0.000000', null, '0.000000', '', '', '', '', '', '', '12.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('58', '客户1', '', '', '', '', null, '客户', '', '0.000000', '200.000000', '0.000000', '-100.000000', null, '', '', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('59', '客户2', '', '', '', '', null, '客户', '', '0.000000', '0.000000', '0.000000', '0.000000', null, '', '', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('60', '12312666', '', '', '', '', null, '会员', '', '1005.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('61', '', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', '12312312.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('62', '供if', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('63', '', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('64', 'wrwer', '', '', '', '', null, '供应商', '', '0.000000', '0.000000', '0.000000', null, '0.000000', '', '', '', '', '', '', '233.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('65', '123123', '', '', '', '', null, '供应商', '', '0.000000', '0.000000', '0.000000', null, '0.000000', '', '', '', '', '', '', '44.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('66', 'rrtt', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('67', '供应商2', '', '', '', '', null, '供应商', '', '0.000000', '0.000000', '0.000000', null, '0.000000', '', '', '', '', '', '', '7.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('68', '供应商3', '', '', '', '', null, '供应商', '', '0.000000', '15.000000', '0.000000', null, '-15.000000', '', '13000000000', '', '', '', '', '22.000000', '63', '0');
INSERT INTO `jsh_supplier` VALUES ('69', '', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', '3123.000000', '63', '1');
INSERT INTO `jsh_supplier` VALUES ('70', 'rrrrr', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('71', '客户3', '', '', '', '', null, '客户', '', '0.000000', '0.000000', '0.000000', '0.000000', null, '', '', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('72', 'sdfafadsf', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('73', 'sadvczXCvz', '', '', '', '', null, '供应商', '', '0.000000', null, null, null, null, '', '', '', '', '', '', null, '63', '1');
INSERT INTO `jsh_supplier` VALUES ('74', '会员1', '会员1', '111111', '111@qq.com', '', null, '会员', '', '0.000000', null, null, null, null, '', '11111111111', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('75', '11111', '1', '1', '11@11.com', '', null, '供应商', '', '0.000000', '1.000000', null, null, null, '', '1', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('76', '111111', '111', '1111', '111@11.com', '', null, '客户', '', '0.000000', null, null, null, null, '1111', '111111', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('77', '东方红水库', '张三', '0451', '12345@qq.com', '', null, '供应商', '', '0.000000', null, null, null, null, '', '18503695595', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('78', '客户1', '1111', '', '111@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '1111', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('79', '供应商2', '2', '', '2@qq.com', '', null, '供应商', '', '0.000000', null, null, null, null, '', '2', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('80', '客户4', '客户4', '', '4444@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '4444', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('81', '会员2', '会员 2', '', '2222@qq.com', '', null, '会员', '', '0.000000', null, null, null, null, '', '22222', '', '', '', '', null, '63', '0');
INSERT INTO `jsh_supplier` VALUES ('82', '供应商鸿兴', '章三', '1111', '254@qq.com', '', null, '供应商', '', '0.000000', null, null, null, null, '', '1234567', '', '', '', '', null, null, '0');
INSERT INTO `jsh_supplier` VALUES ('83', '洪光农场', '黎明', '0451', '254@qq.com', '', null, '供应商', '', '0.000000', null, null, null, null, '', '12334', '', '', '', '', null, '134', '0');
INSERT INTO `jsh_supplier` VALUES ('84', '123', '李佳忆', '0154191', '2547@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '18503695595', '', '', '', '', null, '134', '1');
INSERT INTO `jsh_supplier` VALUES ('85', '一号农场', '李晓华', '123246', '254@qq.com', '', null, '供应商', '\0', '0.000000', null, null, null, null, '', '1850367', '', '', '', '', null, null, '0');
INSERT INTO `jsh_supplier` VALUES ('86', '客户6', '张国', '123', '123@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '2137687', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('87', '阳光农场', '李晓丽', '0154', '2454@qq.com', '', null, '供应商', '', '0.000000', null, null, null, null, '', '12344', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('88', '三号农场', '李三', '01523', '123@qq.com', '', null, '供应商', '\0', '0.000000', null, null, null, null, '', '1232324', '', '', '', '', null, null, '0');
INSERT INTO `jsh_supplier` VALUES ('89', '客户8', '梨花', '', '3454@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '12324', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('90', 'test', '联系人1', '', '', '', null, '会员', '', '0.000000', null, null, null, null, '', '123123', '', '', '', '', null, null, '0');
INSERT INTO `jsh_supplier` VALUES ('91', '阳光农场', '李思', '0123', '123@qq.com', '', null, '供应商', '\0', '0.000000', null, null, null, null, '', '1237634', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('92', '用户', '客户9', '123123', '123@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '123123', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('93', '五号农场', '李华华', '2131123', '254@qq.com', '', null, '供应商', '\0', '0.000000', null, null, null, null, '', '123123', '', '', '', '', null, null, '1');
INSERT INTO `jsh_supplier` VALUES ('94', '客户11', '客户11', '23', '253@qq.com', '', null, '客户', '', '0.000000', null, null, null, null, '', '123123', '', '', '', '', null, null, '1');

-- ----------------------------
-- Table structure for `jsh_systemconfig`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_systemconfig`;
CREATE TABLE `jsh_systemconfig` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `company_name` varchar(50) DEFAULT NULL COMMENT '公司名称',
  `company_contacts` varchar(20) DEFAULT NULL COMMENT '公司联系人',
  `company_address` varchar(50) DEFAULT NULL COMMENT '公司地址',
  `company_tel` varchar(20) DEFAULT NULL COMMENT '公司电话',
  `company_fax` varchar(20) DEFAULT NULL COMMENT '公司传真',
  `company_post_code` varchar(20) DEFAULT NULL COMMENT '公司邮编',
  `depot_flag` varchar(1) DEFAULT '0' COMMENT '仓库启用标记，0未启用，1启用',
  `customer_flag` varchar(1) DEFAULT '0' COMMENT '客户启用标记，0未启用，1启用',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统参数';

-- ----------------------------
-- Records of jsh_systemconfig
-- ----------------------------
INSERT INTO `jsh_systemconfig` VALUES ('7', '南通jshERP公司', '张三', '南通市通州区某某路', '0513-10101010', '0513-18181818', '226300', '0', '0', null, '0');
INSERT INTO `jsh_systemconfig` VALUES ('8', '公司123', '', '', '', '', '', '0', '0', '117', '0');
INSERT INTO `jsh_systemconfig` VALUES ('9', '公司1', '小军', '', '', '', '', '1', '1', '63', '0');

-- ----------------------------
-- Table structure for `jsh_tenant`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_tenant`;
CREATE TABLE `jsh_tenant` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `login_name` varchar(255) DEFAULT NULL COMMENT '登录名',
  `user_num_limit` int(11) DEFAULT NULL COMMENT '用户数量限制',
  `bills_num_limit` int(11) DEFAULT NULL COMMENT '单据数量限制',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8 COMMENT='租户';

-- ----------------------------
-- Records of jsh_tenant
-- ----------------------------
INSERT INTO `jsh_tenant` VALUES ('13', '63', 'jsh', '20', '2000', null);
INSERT INTO `jsh_tenant` VALUES ('14', '113', 'abc123', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('15', '115', 'jzh', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('16', '123', 'caoyuli', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('17', '124', 'jchb', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('18', '126', '123123', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('19', '127', '2345123', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('20', '128', 'q12341243', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('21', '130', 'jsh666', '2', '200', null);
INSERT INTO `jsh_tenant` VALUES ('81', '134', '18503695595', '5', '200', '2020-07-08 09:26:46');
INSERT INTO `jsh_tenant` VALUES ('82', '135', '18236588028', '5', '200', '2020-07-08 11:01:26');
INSERT INTO `jsh_tenant` VALUES ('83', '136', '18236588028', '5', '200', '2020-07-08 11:38:54');
INSERT INTO `jsh_tenant` VALUES ('84', '139', '18236588028', '5', '200', '2020-07-08 12:17:47');

-- ----------------------------
-- Table structure for `jsh_unit`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_unit`;
CREATE TABLE `jsh_unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `UName` varchar(50) DEFAULT NULL COMMENT '名称，支持多单位',
  `basic_unit` varchar(50) DEFAULT NULL COMMENT '基础单位',
  `other_unit` varchar(50) DEFAULT NULL COMMENT '副单位',
  `ratio` int(11) DEFAULT NULL COMMENT '比例',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COMMENT='多单位表';

-- ----------------------------
-- Records of jsh_unit
-- ----------------------------
INSERT INTO `jsh_unit` VALUES ('2', 'kg,包(1:25)', 'kg', '包', '25', null, '0');
INSERT INTO `jsh_unit` VALUES ('8', '瓶,箱(1:12)', '瓶', '箱', '12', null, '0');
INSERT INTO `jsh_unit` VALUES ('14', '个,箱(1:12)', '个', '箱', '12', '117', '0');
INSERT INTO `jsh_unit` VALUES ('15', '个,箱(1:12)', '个', '箱', '12', '63', '0');
INSERT INTO `jsh_unit` VALUES ('16', '1,1(1:1)', '1', '1', '1', '63', '1');
INSERT INTO `jsh_unit` VALUES ('17', '千克,吨(1:1000)', '千克', '吨', '1000', '63', '0');

-- ----------------------------
-- Table structure for `jsh_user`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_user`;
CREATE TABLE `jsh_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(255) NOT NULL COMMENT '用户姓名--例如张三',
  `login_name` varchar(255) NOT NULL COMMENT '登录用户名',
  `password` varchar(50) DEFAULT NULL COMMENT '登陆密码',
  `position` varchar(200) DEFAULT NULL COMMENT '职位',
  `department` varchar(255) DEFAULT NULL COMMENT '所属部门',
  `email` varchar(100) DEFAULT NULL COMMENT '电子邮箱',
  `phonenum` varchar(100) DEFAULT NULL COMMENT '手机号码',
  `ismanager` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否为管理者 0==管理者 1==员工',
  `isystem` tinyint(4) NOT NULL DEFAULT '0' COMMENT '是否系统自带数据 ',
  `Status` tinyint(4) DEFAULT '0' COMMENT '状态，0：正常，1：删除，2封禁',
  `description` varchar(500) DEFAULT NULL COMMENT '用户描述信息',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  `tenant_id` bigint(20) DEFAULT NULL COMMENT '租户id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of jsh_user
-- ----------------------------
INSERT INTO `jsh_user` VALUES ('120', '管理员', 'admin', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, '1', '1', '0', null, null, null);
INSERT INTO `jsh_user` VALUES ('134', '李佳忆', 'ljy', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, '1', '1', '0', null, null, null);
INSERT INTO `jsh_user` VALUES ('139', '张三', 'zhangsan', 'e10adc3949ba59abbe56e057f20f883e', null, null, null, null, '1', '0', '0', null, null, '139');
INSERT INTO `jsh_user` VALUES ('140', '123456', 'test', 'e10adc3949ba59abbe56e057f20f883e', '', null, '', '', '1', '0', '0', '', null, '139');
INSERT INTO `jsh_user` VALUES ('141', 'test2', 'test2', 'e10adc3949ba59abbe56e057f20f883e', '', null, '', '', '1', '0', '0', '', null, '139');

-- ----------------------------
-- Table structure for `jsh_userbusiness`
-- ----------------------------
DROP TABLE IF EXISTS `jsh_userbusiness`;
CREATE TABLE `jsh_userbusiness` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `Type` varchar(50) DEFAULT NULL COMMENT '类别',
  `KeyId` varchar(50) DEFAULT NULL COMMENT '主ID',
  `Value` varchar(10000) DEFAULT NULL COMMENT '值',
  `BtnStr` varchar(2000) DEFAULT NULL COMMENT '按钮权限',
  `delete_Flag` varchar(1) DEFAULT '0' COMMENT '删除标记，0未删除，1删除',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8 COMMENT='用户/角色/模块关系表';

-- ----------------------------
-- Records of jsh_userbusiness
-- ----------------------------
INSERT INTO `jsh_userbusiness` VALUES ('5', 'RoleFunctions', '4', '[245][13][12][16][243][14][15][234][236][22][23][220][240][25][217][218][26][194][195][31][59][207][208][209][226][227][228][229][235][237][244][210][211][241][33][199][242][41][200][201][202][40][232][233][197][203][204][205][206][212][246][245]', '[{\"funId\":\"25\",\"btnStr\":\"1\"},{\"funId\":\"217\",\"btnStr\":\"1\"},{\"funId\":\"218\",\"btnStr\":\"1\"},{\"funId\":\"241\",\"btnStr\":\"3\"},{\"funId\":\"242\",\"btnStr\":\"3\"}]', '0');
INSERT INTO `jsh_userbusiness` VALUES ('6', 'RoleFunctions', '5', '[22][23][25][26][194][195][31][33][200][201][41][199][202]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('7', 'RoleFunctions', '6', '[22][23][220][240][25][217][218][26][194][195][31][59][207][208][209][226][227][228][229][235][237][210][211][241][33][199][242][41][200][201][202][40][232][233][197][203][204][205][206][212]', '[{\"funId\":\"33\",\"btnStr\":\"4\"}]', '0');
INSERT INTO `jsh_userbusiness` VALUES ('9', 'RoleFunctions', '7', '[168][13][12][16][14][15][189][18][19][132]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('10', 'RoleFunctions', '8', '[168][13][12][16][14][15][189][18][19][132][22][23][25][26][27][157][158][155][156][125][31][127][126][128][33][34][35][36][37][39][40][41][42][43][46][47][48][49][50][51][52][53][54][55][56][57][192][59][60][61][62][63][65][66][68][69][70][71][73][74][76][77][79][191][81][82][83][85][89][161][86][176][165][160][28][134][91][92][29][94][95][97][104][99][100][101][102][105][107][108][110][111][113][114][116][117][118][120][121][131][135][123][122][20][130][146][147][138][148][149][153][140][145][184][152][143][170][171][169][166][167][163][164][172][173][179][178][181][182][183][186][187]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('11', 'RoleFunctions', '9', '[168][13][12][16][14][15][189][18][19][132][22][23][25][26][27][157][158][155][156][125][31][127][126][128][33][34][35][36][37][39][40][41][42][43][46][47][48][49][50][51][52][53][54][55][56][57][192][59][60][61][62][63][65][66][68][69][70][71][73][74][76][77][79][191][81][82][83][85][89][161][86][176][165][160][28][134][91][92][29][94][95][97][104][99][100][101][102][105][107][108][110][111][113][114][116][117][118][120][121][131][135][123][122][20][130][146][147][138][148][149][153][140][145][184][152][143][170][171][169][166][167][163][164][172][173][179][178][181][182][183][186][187][188]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('12', 'UserRole', '1', '[5]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('13', 'UserRole', '2', '[6][7]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('14', 'UserDepot', '2', '[1][2][6][7]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('15', 'UserDepot', '1', '[1][2][5][6][7][10][12][14][15][17]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('16', 'UserRole', '63', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('18', 'UserDepot', '63', '[14][15]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('19', 'UserDepot', '5', '[6][45][46][50]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('20', 'UserRole', '5', '[5]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('21', 'UserRole', '64', '[13]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('22', 'UserDepot', '64', '[1]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('23', 'UserRole', '65', '[5]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('24', 'UserDepot', '65', '[1]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('25', 'UserCustomer', '64', '[5][2]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('26', 'UserCustomer', '65', '[6]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('27', 'UserCustomer', '63', '[58]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('28', 'UserDepot', '96', '[7]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('29', 'UserRole', '96', '[6]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('30', 'UserRole', '113', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('32', 'RoleFunctions', '10', '[210][211][241][33][199][242][41][200][201][202][40][232][233][197][203][204][205][206][212][59][207][208][209][226][227][228][229][235][237][244][22][23][220][240][25][217][218][26][194][195][31][13][243][14][15][234]', '[{\"funId\":\"25\",\"btnStr\":\"1\"},{\"funId\":\"217\",\"btnStr\":\"1\"},{\"funId\":\"218\",\"btnStr\":\"1\"},{\"funId\":\"241\",\"btnStr\":\"3\"},{\"funId\":\"242\",\"btnStr\":\"3\"}]', '0');
INSERT INTO `jsh_userbusiness` VALUES ('34', 'UserRole', '115', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('35', 'UserRole', '117', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('36', 'UserDepot', '117', '[8][9]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('37', 'UserCustomer', '117', '[52]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('38', 'UserRole', '120', '[4]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('39', 'UserDepot', '120', '[7][8][9][10][11][12][2][1][3]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('40', 'UserCustomer', '120', '[52][48][6][5][2]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('41', 'RoleFunctions', '12', '', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('48', 'RoleFunctions', '13', '[59][207][208][209][226][227][228][229][235][237][210][211][241][33][199][242][41][200]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('51', 'UserRole', '74', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('52', 'UserDepot', '121', '[13]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('54', 'UserDepot', '115', '[13]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('56', 'UserCustomer', '115', '[56]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('57', 'UserCustomer', '121', '[56]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('58', 'UserRole', '121', '[15]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('59', 'UserRole', '123', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('60', 'UserRole', '124', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('61', 'UserRole', '125', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('62', 'UserRole', '126', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('63', 'UserRole', '127', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('64', 'UserRole', '128', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('65', 'UserRole', '129', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('66', 'UserRole', '130', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('67', 'UserRole', '131', '[16]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('68', 'UserRole', '133', '[4]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('69', 'RoleFunctions', '16', '', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('70', 'UserRole', '134', '[4]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('71', 'RoleFunctions', '18', '[210][211][241][33][199][242][41][200][201][202][40][232][233][197][203][204][205][206][212][59][207][208][209][226][227][228][229][235][237][244][22][23][220][240][25][217][218][26][194][195][31]', '[{\"funId\":\"25\",\"btnStr\":\"1,2\"},{\"funId\":\"217\",\"btnStr\":\"1,2\"},{\"funId\":\"218\",\"btnStr\":\"1,2\"},{\"funId\":\"201\",\"btnStr\":\"3,4,5\"},{\"funId\":\"202\",\"btnStr\":\"3,4,5\"},{\"funId\":\"40\",\"btnStr\":\"3,4,5\"},{\"funId\":\"232\",\"btnStr\":\"3,4,5\"},{\"funId\":\"233\",\"btnStr\":\"3,4,5\"}]', '0');
INSERT INTO `jsh_userbusiness` VALUES ('72', 'UserRole', '135', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('73', 'UserRole', '136', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('74', 'UserRole', '139', '[10]', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('75', 'UserRole', '140', '', null, '0');
INSERT INTO `jsh_userbusiness` VALUES ('76', 'UserRole', '141', '[5]', null, '0');
