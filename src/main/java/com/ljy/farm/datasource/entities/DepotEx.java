package com.ljy.farm.datasource.entities;

import lombok.Data;

import java.math.BigDecimal;


@Data
public class DepotEx extends Depot {

    private String principalName;

    private BigDecimal stock;

    public String getPrincipalName() {
        return principalName;
    }

    public void setPrincipalName(String principalName) {
        this.principalName = principalName;
    }

    public BigDecimal getStock() {
        return stock;
    }

    public void setStock(BigDecimal stock) {
        this.stock = stock;
    }
}
