package com.ljy.farm.datasource.mappers;

import com.ljy.farm.datasource.entities.Supplier;
import com.ljy.farm.datasource.entities.SupplierExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface SupplierMapper {

    int countByExample(SupplierExample example);

    int deleteByExample(SupplierExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Supplier record);

    int insertSelective(Supplier record);

    List<Supplier> selectByExample(SupplierExample example);

    Supplier selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Supplier record, @Param("example") SupplierExample example);

    int updateByExample(@Param("record") Supplier record, @Param("example") SupplierExample example);

    int updateByPrimaryKeySelective(Supplier record);

    int updateByPrimaryKey(Supplier record);
}