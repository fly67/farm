package com.ljy.farm.datasource.mappers;

import com.ljy.farm.datasource.entities.OrgaUserRel;


public interface OrgaUserRelMapperEx {

    int addOrgaUserRel(OrgaUserRel orgaUserRel);

    int updateOrgaUserRel(OrgaUserRel orgaUserRel);
}
