package com.ljy.farm.utils;

public class BaseResponseInfo {
    public int code;
    public Object data;

    public BaseResponseInfo() {
        code = 400;
        data = null;
    }
}
