package com.ljy.farm.service.tenant;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;


@ResourceInfo(value = "tenant")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TenantResource {
}
