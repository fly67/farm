package com.ljy.farm.service.msg;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;


@ResourceInfo(value = "msg")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MsgResource {
}
