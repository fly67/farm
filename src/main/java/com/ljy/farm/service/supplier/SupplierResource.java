package com.ljy.farm.service.supplier;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;


@ResourceInfo(value = "supplier")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SupplierResource {
}
