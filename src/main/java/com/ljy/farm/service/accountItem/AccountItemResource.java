package com.ljy.farm.service.accountItem;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;

@ResourceInfo(value = "accountItem")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountItemResource {
}
