package com.ljy.farm.service.unit;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;

@ResourceInfo(value = "unit")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface UnitResource {
}
