package com.ljy.farm.service.systemConfig;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;


@ResourceInfo(value = "systemConfig")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemConfigResource {
}
