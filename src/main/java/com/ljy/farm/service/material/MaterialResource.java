package com.ljy.farm.service.material;

import com.ljy.farm.service.ResourceInfo;

import java.lang.annotation.*;


@ResourceInfo(value = "material")
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface MaterialResource {
}
