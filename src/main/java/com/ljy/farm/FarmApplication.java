package com.ljy.farm;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@MapperScan(basePackages = {"com.ljy.farm.datasource.mappers"})
@ServletComponentScan
@EnableScheduling
public class FarmApplication {
    public static void main(String[] args) {
        SpringApplication.run(FarmApplication.class, args);
    }
}
